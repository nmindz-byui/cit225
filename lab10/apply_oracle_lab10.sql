-- ------------------------------------------------------------------
--  Program Name:   apply_oracle_lab10.sql
--  Lab Assignment: Lab #10
--  Program Author: Michael McLaughlin
--  Creation Date:  02-Mar-2018
-- ------------------------------------------------------------------
-- Instructions:
-- ------------------------------------------------------------------
-- The two scripts contain spooling commands, which is why there
-- isn't a spooling command in this script. When you run this file
-- you first connect to the Oracle database with this syntax:
--
--   sqlplus student/student@xe
--
-- Then, you call this script with the following syntax:
--
--   sql> @apply_oracle_lab10.sql
--
-- ------------------------------------------------------------------

-- Run the prior lab script.
@/home/student/Data/cit225/oracle/lab9/apply_oracle_lab9.sql

-- Open log file.  
SPOOL apply_oracle_lab10.txt

-- --------------------------------------------------------
--  Step #1
--  -------
--  Write a query that returns the rows necessary to
--  insert records into the RENTAL table.
-- --------------------------------------------------------

SET NULL '<Null>'
COLUMN rental_id        FORMAT 9999 HEADING "Rental|ID #"
COLUMN customer         FORMAT 9999 HEADING "Customer|ID #"
COLUMN check_out_date   FORMAT A9   HEADING "Check Out|Date"
COLUMN return_date      FORMAT A10  HEADING "Return|Date"
COLUMN created_by       FORMAT 9999 HEADING "Created|By"
COLUMN creation_date    FORMAT A10  HEADING "Creation|Date"
COLUMN last_updated_by  FORMAT 9999 HEADING "Last|Update|By"
COLUMN last_update_date FORMAT A10  HEADING "Last|Updated"

SELECT   DISTINCT
         R.RENTAL_ID
,        C.CONTACT_ID
,        TU.CHECK_OUT_DATE AS CHECK_OUT_DATE
,        TU.RETURN_DATE AS RETURN_DATE
,        1001 AS created_by
,        TRUNC(SYSDATE) AS creation_date
,        1001 AS last_updated_by
,        TRUNC(SYSDATE) AS last_update_date
FROM     MEMBER M INNER JOIN TRANSACTION_UPLOAD TU
ON       M.ACCOUNT_NUMBER = TU.ACCOUNT_NUMBER INNER JOIN CONTACT C
ON       M.MEMBER_ID = C.MEMBER_ID LEFT JOIN RENTAL R
ON       C.CONTACT_ID = R.CUSTOMER_ID
WHERE    C.FIRST_NAME = TU.FIRST_NAME
AND      NVL(C.MIDDLE_NAME,'x') = NVL(TU.MIDDLE_NAME,'x')
AND      C.LAST_NAME = TU.LAST_NAME
ORDER BY C.CONTACT_ID;

INSERT INTO rental
-- When using NVL(r.rental_id,rental_s1.NEXTVAL) we had a violation in PK_RENTAL_1
-- I troubleshooted that issue for longer than 8 hours to no avail.
-- The only non-obtrusive, 10 week-long backward scripting solution I could find was this.
SELECT   rental_s1.NEXTVAL AS rental_id
,        r.contact_id
,        r.check_out_date
,        r.return_date
,        r.created_by
,        r.creation_date
,        r.last_updated_by
,        r.last_update_date
FROM    (SELECT   DISTINCT
         R.RENTAL_ID
,        C.CONTACT_ID
,        TU.CHECK_OUT_DATE AS CHECK_OUT_DATE
,        TU.RETURN_DATE AS RETURN_DATE
,        1001 AS created_by
,        TRUNC(SYSDATE) AS creation_date
,        1001 AS last_updated_by
,        TRUNC(SYSDATE) AS last_update_date
FROM     MEMBER M INNER JOIN TRANSACTION_UPLOAD TU
ON       M.ACCOUNT_NUMBER = TU.ACCOUNT_NUMBER INNER JOIN CONTACT C
ON       M.MEMBER_ID = C.MEMBER_ID LEFT JOIN RENTAL R
ON       C.CONTACT_ID = R.CUSTOMER_ID
WHERE    C.FIRST_NAME = TU.FIRST_NAME
AND      NVL(C.MIDDLE_NAME,'x') = NVL(TU.MIDDLE_NAME,'x')
AND      C.LAST_NAME = TU.LAST_NAME
ORDER BY C.CONTACT_ID) r;

-- Test
COL rental_count FORMAT 999,999 HEADING "Rental|after|Count"
SELECT   COUNT(*) AS rental_count
FROM     rental;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Write a query that returns the rows necessary to
--  insert records into the RENTAL_ITEM table.
-- --------------------------------------------------------

-- Query headers
SET NULL '<Null>'
COLUMN rental_item_id     FORMAT 99999 HEADING "Rental|Item ID #"
COLUMN rental_id          FORMAT 99999 HEADING "Rental|ID #"
COLUMN item_id            FORMAT 99999 HEADING "Item|ID #"
COLUMN rental_item_price  FORMAT 99999 HEADING "Rental|Item|Price"
COLUMN rental_item_type   FORMAT 99999 HEADING "Rental|Item|Type"

-- The select query
SELECT ri.rental_item_id,
       r.rental_id,
       tu.transaction_amount,
       cl.common_lookup_id AS rental_item_type
    FROM member m INNER JOIN contact c
    ON c.member_id = m.member_id INNER JOIN transaction_upload tu
    ON
    (
        (tu.account_number = m.account_number)
        AND (tu.first_name = c.first_name)
        AND (NVL(tu.middle_name, 'x') = NVL(c.middle_name, 'x'))
        AND (tu.last_name = c.last_name)
    ) LEFT JOIN rental r
    ON (r.check_out_date = tu.check_out_date)
        AND (r.return_date = tu.return_date)
        AND (c.contact_id = r.customer_id) INNER JOIN common_lookup cl
    ON ((tu.rental_item_type = cl.common_lookup_type)
        AND (cl.common_lookup_column = 'RENTAL_ITEM_TYPE')) LEFT JOIN rental_item ri
    ON ((ri.rental_id = r.rental_id)
    AND (ri.item_id = tu.item_id));

SELECT   COUNT(*) AS "Rental Item Before Count"
FROM     rental_item;

-- Insert new rows into RENTAL_ITEM
INSERT INTO rental_item
(
    rental_item_id
,   rental_id
,   item_id
,   created_by
,   creation_date
,   last_updated_by
,   last_update_date
,   rental_item_type
,   rental_item_price
)   SELECT   rental_item_s1.NEXTVAL
           , r.rental_id
           , tu.item_id
           , 1
           , SYSDATE
           , 1
           , SYSDATE
           , cl.common_lookup_id AS rental_item_type
           , tu.transaction_amount
        FROM member m INNER JOIN contact c
        ON c.member_id = m.member_id INNER JOIN transaction_upload tu
        ON
        (
            (tu.account_number = m.account_number)
            AND (tu.first_name = c.first_name)
            AND (NVL(tu.middle_name, 'x') = NVL(c.middle_name, 'x'))
            AND (tu.last_name = c.last_name)
        ) LEFT JOIN rental r 
        ON (r.check_out_date = tu.check_out_date) 
            AND (r.return_date = tu.return_date)
            AND (c.contact_id = r.customer_id) INNER JOIN common_lookup cl
        ON
        (
            (tu.rental_item_type = cl.common_lookup_type)
            AND (cl.common_lookup_column = 'RENTAL_ITEM_TYPE')
        ) LEFT JOIN rental_item ri
        ON
        (
            (ri.rental_id = r.rental_id)
            AND (ri.item_id = tu.item_id)
        );

-- Counts the result set
SELECT   COUNT(*) AS "Rental Item After Count"
FROM     rental_item;

-- --------------------------------------------------------
--  Step #3
--  -------
--  Write a query that returns the rows necessary to
--  insert records into the TRANSACTION table
-- --------------------------------------------------------

-- The select query
SELECT
    transaction_id
,   tu.payment_account_number AS payment_account_number
,   cl1.common_lookup_id AS transaction_type
,   tu.transaction_date AS transaction_date
,   sum(tu.transaction_amount)/1.06 AS transaction_amount
,   r.rental_id
,   cl2.common_lookup_id AS payment_method_type
,   m.credit_card_number AS payment_account_number
    FROM member m INNER JOIN contact c
        ON m.member_id = c.member_id INNER JOIN transaction_upload tu
        ON tu.account_number = m.account_number
            AND tu.first_name = c.first_name
            AND NVL(tu.middle_name, 'x') = NVL(c.middle_name, 'x')
            AND tu.last_name = c.last_name INNER JOIN rental r
        ON c.contact_id = r.customer_id
            AND tu.check_out_date = r.check_out_date
            AND tu.return_date = r.return_date INNER JOIN common_lookup cl1
        ON cl1.common_lookup_table = 'TRANSACTION'
            AND cl1.common_lookup_column = 'TRANSACTION_TYPE'
            AND cl1.common_lookup_type = tu.transaction_type INNER JOIN common_lookup cl2
        ON cl2.common_lookup_table = 'TRANSACTION'
            AND cl2.common_lookup_column = 'PAYMENT_METHOD_TYPE'
            AND cl2.common_lookup_type = tu.payment_method_type LEFT JOIN transaction t
        ON t.transaction_account = tu.payment_account_number
            AND t.transaction_type = cl1.common_lookup_id
            AND t.transaction_date = tu.transaction_date
            AND t.transaction_amount = tu.transaction_amount
            AND t.payment_method_type = cl2.common_lookup_id
            AND t.payment_account_number = m.credit_card_number
        GROUP BY
        (t.transaction_id
,        tu.payment_account_number
,        cl1.common_lookup_id
,        tu.transaction_date
,        r.rental_id
,        cl2.common_lookup_id
,        m.credit_card_number);

-- Insert new rows into TRANSACTION
INSERT INTO transaction
    SELECT transaction_s1.NEXTVAL
,   ni.transaction_account
,   ni.transaction_type
,   ni.transaction_date
,   ni.transaction_amount
,   ni.rental_id
,   ni.payment_method_type
,   ni.payment_account_number
,   1
,   SYSDATE
,   1
,   SYSDATE
    FROM
    (
        SELECT
        transaction_id
        ,   tu.payment_account_number AS transaction_account
        ,   cl1.common_lookup_id AS transaction_type
        ,   tu.transaction_date AS transaction_date
        ,   sum(tu.transaction_amount)/1.06 AS transaction_amount
        ,   r.rental_id
        ,   cl2.common_lookup_id AS payment_method_type
        ,   m.credit_card_number AS payment_account_number
            FROM member m INNER JOIN contact c
                ON m.member_id = c.member_id INNER JOIN transaction_upload tu
                ON tu.account_number = m.account_number
                    AND tu.first_name = c.first_name
                    AND NVL(tu.middle_name, 'x') = NVL(c.middle_name, 'x')
                    AND tu.last_name = c.last_name INNER JOIN rental r
                ON c.contact_id = r.customer_id
                    AND tu.check_out_date = r.check_out_date
                    AND tu.return_date = r.return_date INNER JOIN common_lookup cl1
                ON cl1.common_lookup_table = 'TRANSACTION'
                    AND cl1.common_lookup_column = 'TRANSACTION_TYPE'
                    AND cl1.common_lookup_type = tu.transaction_type INNER JOIN common_lookup cl2
                ON cl2.common_lookup_table = 'TRANSACTION'
                    AND cl2.common_lookup_column = 'PAYMENT_METHOD_TYPE'
                    AND cl2.common_lookup_type = tu.payment_method_type LEFT JOIN transaction t
                ON t.transaction_account = tu.payment_account_number
                    AND t.transaction_type = cl1.common_lookup_id
                    AND t.transaction_date = tu.transaction_date
                    AND t.transaction_amount = tu.transaction_amount
                    AND t.payment_method_type = cl2.common_lookup_id
                    AND t.payment_account_number = m.credit_card_number
        GROUP BY
        (t.transaction_id
        , tu.payment_account_number
        , cl1.common_lookup_id
        , tu.transaction_date
        , r.rental_id
        , cl2.common_lookup_id
        , m.credit_card_number)
    ) ni;

-- Close log file.
SPOOL OFF
 
-- Make all changes permanent.
COMMIT;