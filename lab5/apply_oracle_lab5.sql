-- ------------------------------------------------------------------
--  Program Name:   apply_oracle_lab5.sql
--  Lab Assignment: N/A
--  Program Author: Michael McLaughlin
--  Creation Date:  17-Jan-2018
-- ------------------------------------------------------------------
--  Change Log:
-- ------------------------------------------------------------------
--  Change Date    Change Reason
-- -------------  ---------------------------------------------------
--  
-- ------------------------------------------------------------------
-- This creates tables, sequences, indexes, and constraints necessary
-- to begin lesson #5. Demonstrates proper process and syntax.
-- ------------------------------------------------------------------
-- Instructions:
-- ------------------------------------------------------------------
-- The two scripts contain spooling commands, which is why there
-- isn't a spooling command in this script. When you run this file
-- you first connect to the Oracle database with this syntax:
--
--   sqlplus student/student@xe
--
-- Then, you call this script with the following syntax:
--
--   sql> @apply_oracle_lab5.sql
--
-- ------------------------------------------------------------------

-- Call library files.
@/home/student/Data/cit225/oracle/lib1/utility/cleanup_oracle.sql
@/home/student/Data/cit225/oracle/lib1/create/create_oracle_store2.sql
@/home/student/Data/cit225/oracle/lib1/preseed/preseed_oracle_store.sql
@/home/student/Data/cit225/oracle/lib1/seed/seeding.sql

-- Open log file.
SPOOL apply_oracle_lab5.txt

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section A
-- --------------------------------------------------------
SELECT MEMBER_ID, CONTACT_ID
    FROM MEMBER
    INNER JOIN CONTACT
    USING (MEMBER_ID);

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section B
-- --------------------------------------------------------
SELECT M.MEMBER_ID, C.CONTACT_ID
    FROM MEMBER M, CONTACT C
    WHERE M.MEMBER_ID = C.MEMBER_ID;

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section C
-- --------------------------------------------------------
SELECT CONTACT_ID, ADDRESS_ID
    FROM CONTACT
    INNER JOIN ADDRESS
    USING (CONTACT_ID);

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section D
-- --------------------------------------------------------
SELECT C.CONTACT_ID, A.ADDRESS_ID
    FROM CONTACT C, ADDRESS A
    WHERE C.CONTACT_ID = A.CONTACT_ID;

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section E
-- --------------------------------------------------------
SELECT ADDRESS_ID, STREET_ADDRESS_ID
    FROM ADDRESS
    INNER JOIN STREET_ADDRESS
    USING (ADDRESS_ID);

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section F
-- --------------------------------------------------------
SELECT A.ADDRESS_ID, S.STREET_ADDRESS_ID
    FROM ADDRESS A, STREET_ADDRESS S
    WHERE A.ADDRESS_ID = S.ADDRESS_ID;

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section H
-- --------------------------------------------------------
SELECT ADDRESS_ID, TELEPHONE_ID
    FROM ADDRESS
    INNER JOIN TELEPHONE
    USING (ADDRESS_ID);

-- --------------------------------------------------------
--  Step #1
--  -------
--  Section A
-- --------------------------------------------------------
SELECT A.ADDRESS_ID, T.TELEPHONE_ID
    FROM ADDRESS A, TELEPHONE T
    WHERE A.ADDRESS_ID = T.ADDRESS_ID;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Section A
-- --------------------------------------------------------
SELECT C.CONTACT_ID, S.SYSTEM_USER_ID
    FROM CONTACT C
    INNER JOIN SYSTEM_USER S
    ON C.CREATED_BY = S.SYSTEM_USER_ID;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Section B
-- --------------------------------------------------------
SELECT C.CONTACT_ID, S.SYSTEM_USER_ID
    FROM CONTACT C, SYSTEM_USER S
    WHERE C.CREATED_BY = S.SYSTEM_USER_ID;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Section C
-- --------------------------------------------------------
SELECT C.CONTACT_ID, S.SYSTEM_USER_ID
    FROM CONTACT C
    INNER JOIN SYSTEM_USER S
    ON C.LAST_UPDATED_BY = S.SYSTEM_USER_ID;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Section D
-- --------------------------------------------------------
SELECT C.CONTACT_ID, S.SYSTEM_USER_ID
    FROM CONTACT C, SYSTEM_USER S
    WHERE C.LAST_UPDATED_BY = S.SYSTEM_USER_ID;

-- --------------------------------------------------------
--  Step #3
--  -------
--  Section A
-- --------------------------------------------------------
COL system_user_id  FORMAT 999999  HEADING "System|User|ID #|--------|Table #1"
COL created_by      FORMAT 999999  HEADING "Created|By|ID #|--------|Table #1"
COL system_user_pk  FORMAT 999999  HEADING "System|User|ID #|--------|Table #2"
SELECT SU1.SYSTEM_USER_ID, SU2.CREATED_BY, SU3.SYSTEM_USER_ID system_user_pk
    FROM SYSTEM_USER SU1
    INNER JOIN SYSTEM_USER SU2
    ON SU1.SYSTEM_USER_ID = SU2.SYSTEM_USER_ID
    INNER JOIN SYSTEM_USER SU3
    ON SU2.CREATED_BY = SU3.SYSTEM_USER_ID;

-- --------------------------------------------------------
--  Step #3
--  -------
--  Section B
-- --------------------------------------------------------
COL system_user_id  FORMAT 999999  HEADING "System|User|ID #|--------|Table #1"
COL last_updated_by  FORMAT 999999  HEADING "Last|Updated|By|ID #|--------|Table #1"
COL system_user_pk  FORMAT 999999  HEADING "System|User|ID #|--------|Table #2"
SELECT SU1.SYSTEM_USER_ID, SU2.LAST_UPDATED_BY, SU3.SYSTEM_USER_ID system_user_pk
    FROM SYSTEM_USER SU1
    INNER JOIN SYSTEM_USER SU2
    ON SU1.SYSTEM_USER_ID = SU2.SYSTEM_USER_ID
    INNER JOIN SYSTEM_USER SU3
    ON SU2.LAST_UPDATED_BY = SU3.SYSTEM_USER_ID;

-- --------------------------------------------------------
--  Step #3
--  -------
--  Section C
-- --------------------------------------------------------
COL user_id        FORMAT 999999  HEADING "System|User|ID #|--------|Table #1"
COL user_name      FORMAT A8      HEADING "System|User|Name|--------|Table #1"
COL cby_user_id    FORMAT 999999  HEADING "System|User|ID #|--------|Table #2"
COL cby_user_name  FORMAT A8      HEADING "System|User|Name|--------|Table #2"
COL lby_user_id    FORMAT 999999  HEADING "System|User|ID #|--------|Table #3"
COL lby_user_name  FORMAT A8      HEADING "System|User|Name|--------|Table #3"
SELECT SU1.SYSTEM_USER_ID user_id, SU1.SYSTEM_USER_NAME user_name,
    SU2.CREATED_BY cby_user_id, SU2.SYSTEM_USER_NAME cby_user_name,
    SU3.LAST_UPDATED_BY lby_user_id, SU3.SYSTEM_USER_NAME lby_user_name
    FROM SYSTEM_USER SU1
    INNER JOIN SYSTEM_USER SU2
    ON SU2.SYSTEM_USER_ID = SU2.CREATED_BY
    INNER JOIN SYSTEM_USER SU3
    ON SU3.SYSTEM_USER_ID = SU3.LAST_UPDATED_BY;

-- --------------------------------------------------------
--  Step #4
--  -------
--  Unique Section
-- --------------------------------------------------------
COL rental_id         FORMAT 999999  HEADING "Rental|Table|--------|Rental|ID #"
COL rental_item_rid   FORMAT 999999  HEADING "Rental Item|Table|-----------|Rental|ID #"
COL rental_item_iid   FORMAT 999999  HEADING "Rental Item|Table|-----------|Item|ID #"
COL item_id           FORMAT 999999  HEADING "Item|Table|--------|Item|ID #"
SELECT RTL.RENTAL_ID rental_id, RIT.RENTAL_ID,
    RIT.ITEM_ID, I.ITEM_ID
    FROM RENTAL RTL
    INNER JOIN RENTAL_ITEM RIT
    ON RTL.RENTAL_ID = RIT.RENTAL_ID
    INNER JOIN ITEM I
    ON RIT.ITEM_ID = I.ITEM_ID;
ALTER TABLE RENTAL
    DROP CONSTRAINT NN_RENTAL_3;

-- --------------------------------------------------------
--  Step #5
--  -------
--  Invokes the required script for the step and runs the desired script
-- --------------------------------------------------------
@/home/student/Data/cit225/oracle/lab5/step5-1.sql

-- Tests for the successful completion of step5-1.sql
SELECT   d.department_name, ROUND(AVG(s.salary),0) salary
    FROM     employee e INNER JOIN department d
    ON       e.department_id = d.department_id INNER JOIN salary s
    ON       e.salary_id = s.salary_id
    GROUP BY d.department_name
    ORDER BY d.department_name;

@/home/student/Data/cit225/oracle/lab5/step5-2.sql

-- Tests for the successful completion of step5-2.sql
/* Set output parameters. */
SET PAGESIZE 16

/* Format column output. */
COL short_month FORMAT A5 HEADING "Short|Month"
COL long_month  FORMAT A9 HEADING "Long|Month"
COL start_date  FORMAT A9 HEADING "Start|Date"
COL end_date    FORMAT A9 HEADING "End|Date" 

/* Query the results from the table. */
SELECT * FROM mock_calendar;

-- The actual query we are supposed to modify and get working (WHERE clause)
SELECT   d.department_name, ROUND(AVG(s.salary),0) salary
    FROM employee e INNER JOIN department d
    ON e.department_id = d.department_id INNER JOIN salary s
    ON e.salary_id = s.salary_id
    WHERE NVL(effective_start_date, TRUNC(SYSDATE)+3) > TRUNC(SYSDATE) - 60
    AND NVL(effective_start_date, TRUNC(SYSDATE)+3) < TRUNC(SYSDATE) + 2
    GROUP BY d.department_name
    ORDER BY d.department_name;

SPOOL OFF
