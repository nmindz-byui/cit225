SQL> 
SQL> -- insert your SQL statements here ...
SQL> -- start with the validation scripts you find in the seeding.sql script.
SQL> -- copying the seeding.sql file and editing it to conform to this layout is the simplest approach to the lab.
SQL> 
SQL> -- @/home/student/Data/cit225/oracle/lab4/group_account1_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/group_account2_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/group_account3_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/item_inserts_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/create_insert_contacts_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/individual_accounts_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/update_members_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/rental_inserts_lab.sql
SQL> -- @/home/student/Data/cit225/oracle/lab4/create_view_lab.sql
SQL> @/home/student/Data/cit225/oracle/lab4/seeding_lab.sql
SQL> -- ------------------------------------------------------------------
SQL> --  Program Name:	 seeding.sql
SQL> --  Lab Assignment: N/A
SQL> --  Program Author: Michael McLaughlin
SQL> --  Creation Date:  30-Jan-2018
SQL> -- ------------------------------------------------------------------
SQL> --  Change Log:
SQL> -- ------------------------------------------------------------------
SQL> --  Change Date	Change Reason
SQL> -- -------------  ---------------------------------------------------
SQL> --
SQL> -- ------------------------------------------------------------------
SQL> --  This seeds data in the video store model.
SQL> --   - Inserts the data in the rental table for 5 records and
SQL> --     then inserts 9 dependent records in a non-sequential
SQL> --     order.
SQL> --   - A non-sequential order requires that you use subqueries
SQL> --     to discover the foreign key values for the inserts.
SQL> -- ------------------------------------------------------------------
SQL> 
SQL> @@group_account1_lab.sql
SQL> -- ------------------------------------------------------------------
SQL> --  Program Name:	 group_account1.sql
SQL> --  Lab Assignment: N/A
SQL> --  Program Author: Michael McLaughlin
SQL> --  Creation Date:  30-Jan-2018
SQL> -- ------------------------------------------------------------------
SQL> --  Change Log:
SQL> -- ------------------------------------------------------------------
SQL> --  Change Date	Change Reason
SQL> -- -------------  ---------------------------------------------------
SQL> --
SQL> -- ------------------------------------------------------------------
SQL> -- This seeds data in the video store model.
SQL> -- ------------------------------------------------------------------
SQL> -- ------------------------------------------------------------------
SQL> --  This seeds rows in a dependency chain, including the following
SQL> --  tables:
SQL> --
SQL> --    1. MEMBER
SQL> --    2. CONTACT
SQL> --    3. ADDRESS
SQL> --    4. STREET_ADDRESS
SQL> --    5. TELEPHONE
SQL> --
SQL> --  It creates primary keys with the .NEXTVAL pseudo columns and
SQL> --  foreign keys with the .CURRVAL pseudo columns.
SQL> -- ------------------------------------------------------------------
SQL> 
SQL> -- ------------------------------------------------------------------
SQL> --  Open log file.
SQL> -- ------------------------------------------------------------------
SQL> SPOOL group_account1_lab.txt
