SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #1
SQL> --  -------
SQL> --  Disable foreign key constraints dependencies.
SQL> -- --------------------------------------------------------
SQL> 
SQL> ALTER TABLE SYSTEM_USER_LAB DISABLE CONSTRAINT FK_SYSTEM_USER_LAB_3;

Table altered.

SQL> ALTER TABLE SYSTEM_USER_LAB DISABLE CONSTRAINT FK_SYSTEM_USER_LAB_4;

Table altered.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #2
SQL> --  -------
SQL> --  Remove the not null constraints
SQL> -- --------------------------------------------------------
SQL> 
SQL> ALTER TABLE SYSTEM_USER_LAB MODIFY (system_user_group_id NULL);

Table altered.

SQL> ALTER TABLE SYSTEM_USER_LAB MODIFY (system_user_type NULL);

Table altered.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #3
SQL> --  -------
SQL> --  Insert one row into the SYSTEM_USER_LAB table
SQL> -- --------------------------------------------------------
SQL> 
SQL> INSERT INTO SYSTEM_USER_LAB VALUES (1, 'SYSADMIN', NULL, NULL, '', '', '', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #4
SQL> --  -------
SQL> --  Disable foreign key constraints dependencies (again).
SQL> -- --------------------------------------------------------
SQL> 
SQL> ALTER TABLE COMMON_LOOKUP_LAB DISABLE CONSTRAINT FK_CLOOKUP_LAB_1;

Table altered.

SQL> ALTER TABLE COMMON_LOOKUP_LAB DISABLE CONSTRAINT FK_CLOOKUP_LAB_2;

Table altered.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #5
SQL> --  -------
SQL> --  Insert 20 rows into COMMON_LOOKUP_LAB
SQL> -- --------------------------------------------------------
SQL> 
SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1, 'SYSTEM_USER_LAB', 'SYSTEM_ADMIN', 'System Administrator', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (2, 'SYSTEM_USER_LAB', 'DBA', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (3, 'SYSTEM_USER_LAB', 'SYSTEM_GROUP', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (4, 'SYSTEM_USER_LAB', 'COST_CENTER', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (5, 'SYSTEM_USER_LAB', 'INDIVIDUAL', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1001, 'CONTACT_LAB', 'EMPLOYEE', 'Employee', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1002, 'CONTACT_LAB', 'CUSTOMER', 'Customer', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1003, 'MEMBER_LAB', 'INDIVIDUAL', 'Individual Membership', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1004, 'MEMBER_LAB', 'GROUP', 'Group Membership', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1005, 'MEMBER_LAB', 'DISCOVER_CARD', 'Discover Card', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1006, 'MEMBER_LAB', 'MASTER_CARD', 'Master Card', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1007, 'MEMBER_LAB', 'VISA_CARD', 'VISA Card', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1008, 'MULTIPLE', 'HOME', 'Home', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1009, 'MULTIPLE', 'WORK', 'Work', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1010, 'ITEM_LAB', 'DVD_FULL_SCREEN', 'DVD: Full Screen', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1011, 'ITEM_LAB', 'DVD_WIDE_SCREEN', 'DVD: Wide Screen', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1012, 'ITEM_LAB', 'NINTENDO_GAMECUBE', 'Nintendo Gamecube', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1013, 'ITEM_LAB', 'PLAYSTATION2', 'PlayStation2', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1014, 'ITEM_LAB', 'XBOX', 'XBox', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> INSERT INTO COMMON_LOOKUP_LAB VALUES (1015, 'ITEM_LAB', 'BLU-RAY', 'Blu-ray', 1, SYSDATE, 1, SYSDATE);

1 row created.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #6
SQL> --  -------
SQL> --  Query the inserted 20 rows into COMMON_LOOKUP_LAB
SQL> -- --------------------------------------------------------
SQL> 
SQL> COL common_lookup_id	FORMAT	9999
SQL> COL common_lookup_context	FORMAT A22
SQL> COL common_lookup_type	FORMAT A18
SQL> SELECT   common_lookup_lab_id
  2  ,	      common_lookup_context
  3  ,	      common_lookup_type
  4  FROM     common_lookup_lab;

 Member                                                                         
 Lookup                        Common                                           
   ID # COMMON_LOOKUP_CONTEXT  Lookup Type                                      
------- ---------------------- ------------------                               
      1 SYSTEM_USER_LAB        SYSTEM_ADMIN                                     
      2 SYSTEM_USER_LAB        DBA                                              
      3 SYSTEM_USER_LAB        SYSTEM_GROUP                                     
      4 SYSTEM_USER_LAB        COST_CENTER                                      
      5 SYSTEM_USER_LAB        INDIVIDUAL                                       
   1001 CONTACT_LAB            EMPLOYEE                                         
   1002 CONTACT_LAB            CUSTOMER                                         
   1003 MEMBER_LAB             INDIVIDUAL                                       
   1004 MEMBER_LAB             GROUP                                            
   1005 MEMBER_LAB             DISCOVER_CARD                                    
   1006 MEMBER_LAB             MASTER_CARD                                      
   1007 MEMBER_LAB             VISA_CARD                                        
   1008 MULTIPLE               HOME                                             
   1009 MULTIPLE               WORK                                             
   1010 ITEM_LAB               DVD_FULL_SCREEN                                  
   1011 ITEM_LAB               DVD_WIDE_SCREEN                                  
   1012 ITEM_LAB               NINTENDO_GAMECUBE                                
   1013 ITEM_LAB               PLAYSTATION2                                     
   1014 ITEM_LAB               XBOX                                             
   1015 ITEM_LAB               BLU-RAY                                          

20 rows selected.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #7
SQL> --  -------
SQL> --  Query data against COMMON_LOOKUP_LAB
SQL> -- --------------------------------------------------------
SQL> 
SQL> SELECT common_lookup_lab_id
  2  	 FROM COMMON_LOOKUP_LAB
  3  	 WHERE common_lookup_context = UPPER('system_user_lab')
  4  	     AND common_lookup_type = UPPER('system_group');

 Member                                                                         
 Lookup                                                                         
   ID #                                                                         
-------                                                                         
      3                                                                         

1 row selected.

SQL> 
SQL> SELECT common_lookup_lab_id
  2  	 FROM COMMON_LOOKUP_LAB
  3  	 WHERE common_lookup_context = UPPER('system_user_lab')
  4  	     AND common_lookup_type = UPPER('system_admin');

 Member                                                                         
 Lookup                                                                         
   ID #                                                                         
-------                                                                         
      1                                                                         

1 row selected.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #8
SQL> --  -------
SQL> --  *(WRONG INSTRUCTIONS)* !!!
SQL> --
SQL> --  Working with sub-queries
SQL> --
SQL> --  *(WRONG INSTRUCTIONS)* !!!
SQL> -- --------------------------------------------------------
SQL> 
SQL> -- Query #1
SQL> UPDATE SYSTEM_USER_LAB
  2  	 SET system_user_type =
  3  	     (SELECT common_lookup_lab_id
  4  		 FROM COMMON_LOOKUP_LAB
  5  		 WHERE common_lookup_context = UPPER('system_user_lab')
  6  		     AND common_lookup_type = UPPER('system_group'));

1 row updated.

SQL> 
SQL> --- Testing Query #1
SQL> SET NULL '<Null>'
SQL> COL system_user_lab_id    FORMAT  9999
SQL> COL system_user_group_id  FORMAT  9999
SQL> COL system_user_type      FORMAT  9999
SQL> COL system_user_name      FORMAT  9999
SQL> SELECT   system_user_lab_id
  2  ,	      system_user_name
  3  ,	      system_user_group_id
  4  ,	      system_user_type
  5  FROM     system_user_lab
  6  WHERE    system_user_lab_id = 1;

                                        System                                  
                   System                 User System                           
                   User                  Group   User                           
SYSTEM_USER_LAB_ID Name                   ID #   Type                           
------------------ -------------------- ------ ------                           
                 1 SYSADMIN             <Null>      3                           

1 row selected.

SQL> 
SQL> -- Query #2
SQL> UPDATE SYSTEM_USER_LAB
  2  	 SET system_user_group_id =
  3  	     (SELECT common_lookup_lab_id
  4  		 FROM COMMON_LOOKUP_LAB
  5  		 WHERE common_lookup_context = UPPER('system_user_lab')
  6  		     AND common_lookup_type = UPPER('system_admin'));

1 row updated.

SQL> 
SQL> -- Testing Query #2
SQL> SET NULL '<Null>'
SQL> COL system_user_lab_id    FORMAT  9999
SQL> COL system_user_group_id  FORMAT  9999
SQL> COL system_user_type      FORMAT  9999
SQL> COL system_user_name      FORMAT  9999
SQL> SELECT   system_user_lab_id
  2  ,	      system_user_name
  3  ,	      system_user_group_id
  4  ,	      system_user_type
  5  FROM     system_user_lab
  6  WHERE    system_user_lab_id = 1;

                                        System                                  
                   System                 User System                           
                   User                  Group   User                           
SYSTEM_USER_LAB_ID Name                   ID #   Type                           
------------------ -------------------- ------ ------                           
                 1 SYSADMIN                  1      3                           

1 row selected.

SQL> 
SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #9
SQL> --  -------
SQL> --  *(WRONG INSTRUCTIONS)* !!!
SQL> --
SQL> --  Re-enable the already enabled FKs and the disabled ones
SQL> --
SQL> --  *(WRONG INSTRUCTIONS)* !!!
SQL> -- --------------------------------------------------------
SQL> 
SQL> -- Query #1
SQL> ALTER TABLE SYSTEM_USER_LAB ENABLE CONSTRAINT FK_SYSTEM_USER_LAB_3;

Table altered.

SQL> ALTER TABLE SYSTEM_USER_LAB ENABLE CONSTRAINT FK_SYSTEM_USER_LAB_4;

Table altered.

SQL> 
SQL> -- Test
SQL> COL table_name	  FORMAT A14  HEADING "Table Name"
SQL> COL constraint_name  FORMAT A24  HEADING "Constraint Name"
SQL> COL constraint_type  FORMAT A15  HEADING "Constraint Type"
SQL> COL status 	  FORMAT A8   HEADING "Status"
SQL> SELECT   table_name
  2  ,	      constraint_name
  3  ,	      CASE
  4  		WHEN constraint_type = 'R'	  THEN 'FOREIGN KEY'
  5  		WHEN constraint_type = 'P'	  THEN 'PRIMARY KEY'
  6  		WHEN constraint_type = 'U'	  THEN 'UNIQUE'
  7  		WHEN constraint_type = 'C' AND	REGEXP_LIKE(constraint_name,'^.*nn.*$','i')
  8  		THEN 'NOT NULL'
  9  		ELSE 'CHECK'
 10  	      END constraint_type
 11  ,	      status
 12  FROM     user_constraints
 13  WHERE    table_name = 'SYSTEM_USER_LAB'
 14  ORDER BY CASE
 15  		WHEN constraint_type = 'PRIMARY KEY' THEN 1
 16  		WHEN constraint_type = 'NOT NULL'    THEN 2
 17  		WHEN constraint_type = 'CHECK'	     THEN 3
 18  		WHEN constraint_type = 'UNIQUE'      THEN 4
 19  		WHEN constraint_type = 'FOREIGN KEY' THEN 5
 20  	      END
 21  ,	      constraint_name;

Table Name     Constraint Name          Constraint Type Status                  
-------------- ------------------------ --------------- --------                
SYSTEM_USER_LA PK_SYSTEM_USER_LAB_1     PRIMARY KEY     ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA NN_SYSTEM_USER_LAB_1     NOT NULL        ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA NN_SYSTEM_USER_LAB_4     NOT NULL        ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA NN_SYSTEM_USER_LAB_5     NOT NULL        ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA NN_SYSTEM_USER_LAB_6     NOT NULL        ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA NN_SYSTEM_USER_LAB_7     NOT NULL        ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA FK_SYSTEM_USER_LAB_1     FOREIGN KEY     ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA FK_SYSTEM_USER_LAB_2     FOREIGN KEY     ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA FK_SYSTEM_USER_LAB_3     FOREIGN KEY     ENABLED                 
B                                                                               
                                                                                
SYSTEM_USER_LA FK_SYSTEM_USER_LAB_4     FOREIGN KEY     ENABLED                 
B                                                                               
                                                                                

10 rows selected.

SQL> 
SQL> -- Query #2
SQL> ALTER TABLE COMMON_LOOKUP_LAB ENABLE CONSTRAINT FK_CLOOKUP_LAB_1;

Table altered.

SQL> ALTER TABLE COMMON_LOOKUP_LAB ENABLE CONSTRAINT FK_CLOOKUP_LAB_2;

Table altered.

SQL> 
SQL> -- Test Query #2
SQL> COL table_name	  FORMAT A14  HEADING "Table Name"
SQL> COL constraint_name  FORMAT A24  HEADING "Constraint Name"
SQL> COL constraint_type  FORMAT A15  HEADING "Constraint Type"
SQL> COL status 	  FORMAT A8   HEADING "Status"
SQL> SELECT   table_name
  2  ,	      constraint_name
  3  ,	      CASE
  4  		WHEN constraint_type = 'R'	  THEN 'FOREIGN KEY'
  5  		WHEN constraint_type = 'P'	  THEN 'PRIMARY KEY'
  6  		WHEN constraint_type = 'U'	  THEN 'UNIQUE'
  7  		WHEN constraint_type = 'C' AND	REGEXP_LIKE(constraint_name,'^.*nn.*$','i')
  8  		THEN 'NOT NULL'
  9  		ELSE 'CHECK'
 10  	      END constraint_type
 11  ,	      status
 12  FROM     user_constraints
 13  WHERE    table_name = 'COMMON_LOOKUP_LAB'
 14  ORDER BY CASE
 15  		WHEN constraint_type = 'PRIMARY KEY' THEN 1
 16  		WHEN constraint_type = 'NOT NULL'    THEN 2
 17  		WHEN constraint_type = 'CHECK'	     THEN 3
 18  		WHEN constraint_type = 'UNIQUE'      THEN 4
 19  		WHEN constraint_type = 'FOREIGN KEY' THEN 5
 20  	      END
 21  ,	      constraint_name;

Table Name     Constraint Name          Constraint Type Status                  
-------------- ------------------------ --------------- --------                
COMMON_LOOKUP_ PK_CLOOKUP_LAB_1         PRIMARY KEY     ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_1         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_2         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_3         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_4         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_5         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_6         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ NN_CLOOKUP_LAB_7         NOT NULL        ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ FK_CLOOKUP_LAB_1         FOREIGN KEY     ENABLED                 
LAB                                                                             
                                                                                
COMMON_LOOKUP_ FK_CLOOKUP_LAB_2         FOREIGN KEY     ENABLED                 
LAB                                                                             
                                                                                

10 rows selected.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #10
SQL> --  -------
SQL> --  *(WRONG INSTRUCTIONS)* !!!
SQL> --
SQL> --  Re-enable NOT NULL constraints on SYSTEM_USER_LAB
SQL> --
SQL> --  *(WRONG INSTRUCTIONS)* !!!
SQL> -- --------------------------------------------------------
SQL> 
SQL> -- Query
SQL> ALTER TABLE SYSTEM_USER_LAB
  2  	 MODIFY (system_user_group_id NUMBER CONSTRAINT nn_system_user_lab_2 NOT NULL);

Table altered.

SQL> 
SQL> ALTER TABLE SYSTEM_USER_LAB
  2  	 MODIFY (system_user_type NUMBER CONSTRAINT nn_system_user_lab_3 NOT NULL);

Table altered.

SQL> 
SQL> -- Testing
SQL> COL table_name	  FORMAT A18  HEADING "Table Name"
SQL> COL constraint_name  FORMAT A28  HEADING "Constraint Name"
SQL> COL constraint_type  FORMAT A15  HEADING "Constraint Type"
SQL> COL status 	  FORMAT A8   HEADING "Status"
SQL> SELECT   table_name
  2  ,	      constraint_name
  3  ,	      CASE
  4  		WHEN constraint_type = 'R'	  THEN 'FOREIGN KEY'
  5  		WHEN constraint_type = 'P'	  THEN 'PRIMARY KEY'
  6  		WHEN constraint_type = 'U'	  THEN 'UNIQUE'
  7  		WHEN constraint_type = 'C' AND	REGEXP_LIKE(constraint_name,'^.*nn.*$','i')
  8  		THEN 'NOT NULL'
  9  		ELSE 'CHECK'
 10  	      END constraint_type
 11  ,	      status
 12  FROM     user_constraints
 13  WHERE    table_name = 'SYSTEM_USER_LAB'
 14  ORDER BY CASE
 15  		WHEN constraint_type = 'PRIMARY KEY' THEN 1
 16  		WHEN constraint_type = 'NOT NULL'    THEN 2
 17  		WHEN constraint_type = 'CHECK'	     THEN 3
 18  		WHEN constraint_type = 'UNIQUE'      THEN 4
 19  		WHEN constraint_type = 'FOREIGN KEY' THEN 5
 20  	      END
 21  ,	      constraint_name;

Table Name         Constraint Name              Constraint Type Status          
------------------ ---------------------------- --------------- --------        
SYSTEM_USER_LAB    PK_SYSTEM_USER_LAB_1         PRIMARY KEY     ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_1         NOT NULL        ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_2         NOT NULL        ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_3         NOT NULL        ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_4         NOT NULL        ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_5         NOT NULL        ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_6         NOT NULL        ENABLED         
SYSTEM_USER_LAB    NN_SYSTEM_USER_LAB_7         NOT NULL        ENABLED         
SYSTEM_USER_LAB    FK_SYSTEM_USER_LAB_1         FOREIGN KEY     ENABLED         
SYSTEM_USER_LAB    FK_SYSTEM_USER_LAB_2         FOREIGN KEY     ENABLED         
SYSTEM_USER_LAB    FK_SYSTEM_USER_LAB_3         FOREIGN KEY     ENABLED         
SYSTEM_USER_LAB    FK_SYSTEM_USER_LAB_4         FOREIGN KEY     ENABLED         

12 rows selected.

SQL> 
SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #11
SQL> --  -------
SQL> --  Re-enable NOT NULL constraints on SYSTEM_USER_LAB
SQL> -- --------------------------------------------------------
SQL> 
SQL> -- Inserting "Phineas Phineas Phineas" - according to the instructions
SQL> -- found at http://michaelmclaughlin.info/db1/week-3-summary/lab-week-4/instructions/
SQL> --
SQL> -- Or more simply, adding SYSTEM_USER_LAB "Phineas Taylor Barnum"
SQL> INSERT INTO SYSTEM_USER_LAB (
  2  	 system_user_lab_id,
  3  	 system_user_type,
  4  	 system_user_group_id,
  5  	 system_user_name,
  6  	 first_name,
  7  	 middle_name,
  8  	 last_name,
  9  	 created_by,
 10  	 creation_date,
 11  	 last_updated_by,
 12  	 last_update_date
 13  )
 14  VALUES (
 15  	 system_user_lab_s1.nextVal,
 16  	 (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('system_group')),
 17  	 (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('dba')),
 18  	 'DBA1',
 19  	 'Phineas',
 20  	 'Taylor',
 21  	 'Barnum',
 22  	 (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
 23  	 SYSDATE,
 24  	 (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
 25  	 SYSDATE
 26  );

1 row created.

SQL> 
SQL> -- Inserting "Phineas '' Phineas" - according to the instructions
SQL> -- found at http://michaelmclaughlin.info/db1/week-3-summary/lab-week-4/instructions/
SQL> --
SQL> -- Or more simply, adding SYSTEM_USER_LAB "Phineas Fogg"
SQL> INSERT INTO SYSTEM_USER_LAB (
  2  	 system_user_lab_id,
  3  	 system_user_type,
  4  	 system_user_group_id,
  5  	 system_user_name,
  6  	 first_name,
  7  	 middle_name,
  8  	 last_name,
  9  	 created_by,
 10  	 creation_date,
 11  	 last_updated_by,
 12  	 last_update_date
 13  )
 14  VALUES (
 15  	 system_user_lab_s1.nextVal,
 16  	 (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('system_group')),
 17  	 (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('dba')),
 18  	 'DBA2',
 19  	 'Phineas',
 20  	 '',
 21  	 'Fogg',
 22  	 (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
 23  	 SYSDATE,
 24  	 (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
 25  	 SYSDATE
 26  );

1 row created.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step #12
SQL> --  -------
SQL> --  Test for Step #11
SQL> -- --------------------------------------------------------
SQL> SET NULL '<Null>'
SQL> COL system_user_id        FORMAT  9999
SQL> COL system_user_group_id  FORMAT  9999
SQL> COL system_user_type      FORMAT  9999
SQL> COL system_user_name      FORMAT  A10
SQL> COL full_user_name        FORMAT  A30
SQL> SELECT   system_user_lab_id
  2  ,	      system_user_group_id
  3  ,	      system_user_type
  4  ,	      system_user_name
  5  ,	      CASE
  6  		WHEN last_name IS NOT NULL THEN
  7  		  last_name || ', ' || first_name || ' ' || middle_name
  8  	      END AS full_user_name
  9  FROM     system_user_lab;

                   System                                                       
                     User System System                                         
                    Group   User User                                           
SYSTEM_USER_LAB_ID   ID #   Type Name       FULL_USER_NAME                      
------------------ ------ ------ ---------- ------------------------------      
                 1      1      3 SYSADMIN   <Null>                              
              1001      2      3 DBA1       Barnum, Phineas Taylor              
              1002      2      3 DBA2       Fogg, Phineas                       

3 rows selected.

SQL> 
SQL> -- --------------------------------------------------------
SQL> --  Step Final
SQL> --  -------
SQL> --  Commit Changes
SQL> -- --------------------------------------------------------
SQL> COMMIT;

Commit complete.

SQL> 
SQL> SPOOL OFF
