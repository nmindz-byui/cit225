-- ------------------------------------------------------------------
--  Program Name:   apply_oracle_lab4.sql
--  Lab Assignment: N/A
--  Program Author: Michael McLaughlin
--  Creation Date:  17-Jan-2018
-- ------------------------------------------------------------------
--  Change Log:
-- ------------------------------------------------------------------
--  Change Date    Change Reason
-- -------------  ---------------------------------------------------
--  
-- ------------------------------------------------------------------
-- This creates tables, sequences, indexes, and constraints necessary
-- to begin lesson #4. Demonstrates proper process and syntax.
-- ------------------------------------------------------------------
-- Instructions:
-- ------------------------------------------------------------------
-- The two scripts contain spooling commands, which is why there
-- isn't a spooling command in this script. When you run this file
-- you first connect to the Oracle database with this syntax:
--
--   sqlplus student/student@xe
--
-- Then, you call this script with the following syntax:
--
--   sql> @apply_oracle_lab4.sql
--
-- ------------------------------------------------------------------

-- Run the prior lab script.
@/home/student/Data/cit225/oracle/lab3/apply_oracle_lab3.sql
@/home/student/Data/cit225/oracle/lib1/seed/seeding.sql
 
-- insert calls to other SQL script files here, in the order that they appear in the seeding.sql script ...
 
SPOOL apply_oracle_lab4.txt
 
-- insert your SQL statements here ... 
-- start with the validation scripts you find in the seeding.sql script.
-- copying the seeding.sql file and editing it to conform to this layout is the simplest approach to the lab. 

-- @/home/student/Data/cit225/oracle/lab4/group_account1_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/group_account2_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/group_account3_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/item_inserts_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/create_insert_contacts_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/individual_accounts_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/update_members_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/rental_inserts_lab.sql
-- @/home/student/Data/cit225/oracle/lab4/create_view_lab.sql
@/home/student/Data/cit225/oracle/lab4/seeding_lab.sql

-- Query/Verify Step #9
-- DESC current_rental_lab


SPOOL OFF
 
-- ------------------------------------------------------------------
--  This is necessary to avoid a resource busy error. You can
--  inadvertently create a resource busy error when testing in two
--  concurrent SQL*Plus sessions unless you provide an explicit
--  COMMIT; statement. 
-- ------------------------------------------------------------------
COMMIT;
