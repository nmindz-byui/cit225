/*
||  Name:          apply_plsql_lab5.sql
||  Date:          11 Nov 2016
||  Purpose:       Complete 325 Chapter 6 lab.
||  Dependencies:  Run the Oracle Database 12c PL/SQL Programming setup programs.
*/

@/home/student/Data/cit325/oracle/lib/cleanup_oracle.sql
@/home/student/Data/cit325/oracle/lib/Oracle12cPLSQLCode/Introduction/create_video_store.sql

-- Open log file.
SPOOL apply_plsql_lab5.txt

-- ... insert your solution here ...

-- Close log file.
SPOOL OFF
