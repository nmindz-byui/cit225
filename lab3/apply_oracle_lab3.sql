-- ------------------------------------------------------------------
--  Program Name:   apply_oracle_lab3.sql
--  Lab Assignment: N/A
--  Program Author: Michael McLaughlin
--  Creation Date:  17-Jan-2018
-- ------------------------------------------------------------------
--  Change Log:
-- ------------------------------------------------------------------
--  Change Date    Change Reason
-- -------------  ---------------------------------------------------
--  
-- ------------------------------------------------------------------
-- This creates tables, sequences, indexes, and constraints necessary
-- to begin lesson #3. Demonstrates proper process and syntax.
-- ------------------------------------------------------------------
-- Instructions:
-- ------------------------------------------------------------------
-- The two scripts contain spooling commands, which is why there
-- isn't a spooling command in this script. When you run this file
-- you first connect to the Oracle database with this syntax:
--
--   sqlplus student/student@xe
--
-- Then, you call this script with the following syntax:
--
--   sql> @apply_oracle_lab3.sql
--
-- ------------------------------------------------------------------

-- Run the prior lab script.
@/home/student/Data/cit225/oracle/lab2/apply_oracle_lab2.sql
@/home/student/Data/cit225/oracle/lib1/preseed/preseed_oracle_store.sql
 
-- ... insert calls to other code script files here ...
 
SPOOL apply_oracle_lab3.txt
 
-- --------------------------------------------------------
--  Step #1
--  -------
--  Disable foreign key constraints dependencies.
-- --------------------------------------------------------

ALTER TABLE SYSTEM_USER_LAB DISABLE CONSTRAINT FK_SYSTEM_USER_LAB_3;
ALTER TABLE SYSTEM_USER_LAB DISABLE CONSTRAINT FK_SYSTEM_USER_LAB_4;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Remove the not null constraints
-- --------------------------------------------------------

ALTER TABLE SYSTEM_USER_LAB MODIFY (system_user_group_id NULL);
ALTER TABLE SYSTEM_USER_LAB MODIFY (system_user_type NULL);

-- --------------------------------------------------------
--  Step #3
--  -------
--  Insert one row into the SYSTEM_USER_LAB table
-- --------------------------------------------------------

INSERT INTO SYSTEM_USER_LAB VALUES (1, 'SYSADMIN', NULL, NULL, '', '', '', 1, SYSDATE, 1, SYSDATE);

-- --------------------------------------------------------
--  Step #4
--  -------
--  Disable foreign key constraints dependencies (again).
-- --------------------------------------------------------

ALTER TABLE COMMON_LOOKUP_LAB DISABLE CONSTRAINT FK_CLOOKUP_LAB_1;
ALTER TABLE COMMON_LOOKUP_LAB DISABLE CONSTRAINT FK_CLOOKUP_LAB_2;

-- --------------------------------------------------------
--  Step #5
--  -------
--  Insert 20 rows into COMMON_LOOKUP_LAB
-- --------------------------------------------------------

INSERT INTO COMMON_LOOKUP_LAB VALUES (1, 'SYSTEM_USER_LAB', 'SYSTEM_ADMIN', 'System Administrator', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (2, 'SYSTEM_USER_LAB', 'DBA', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (3, 'SYSTEM_USER_LAB', 'SYSTEM_GROUP', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (4, 'SYSTEM_USER_LAB', 'COST_CENTER', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (5, 'SYSTEM_USER_LAB', 'INDIVIDUAL', 'Database Administrator', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1001, 'CONTACT_LAB', 'EMPLOYEE', 'Employee', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1002, 'CONTACT_LAB', 'CUSTOMER', 'Customer', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1003, 'MEMBER_LAB', 'INDIVIDUAL', 'Individual Membership', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1004, 'MEMBER_LAB', 'GROUP', 'Group Membership', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1005, 'MEMBER_LAB', 'DISCOVER_CARD', 'Discover Card', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1006, 'MEMBER_LAB', 'MASTER_CARD', 'Master Card', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1007, 'MEMBER_LAB', 'VISA_CARD', 'VISA Card', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1008, 'MULTIPLE', 'HOME', 'Home', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1009, 'MULTIPLE', 'WORK', 'Work', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1010, 'ITEM_LAB', 'DVD_FULL_SCREEN', 'DVD: Full Screen', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1011, 'ITEM_LAB', 'DVD_WIDE_SCREEN', 'DVD: Wide Screen', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1012, 'ITEM_LAB', 'NINTENDO_GAMECUBE', 'Nintendo Gamecube', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1013, 'ITEM_LAB', 'PLAYSTATION2', 'PlayStation2', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1014, 'ITEM_LAB', 'XBOX', 'XBox', 1, SYSDATE, 1, SYSDATE);
INSERT INTO COMMON_LOOKUP_LAB VALUES (1015, 'ITEM_LAB', 'BLU-RAY', 'Blu-ray', 1, SYSDATE, 1, SYSDATE);

-- --------------------------------------------------------
--  Step #6
--  -------
--  Query the inserted 20 rows into COMMON_LOOKUP_LAB
-- --------------------------------------------------------

COL common_lookup_id       FORMAT  9999
COL common_lookup_context  FORMAT A22
COL common_lookup_type     FORMAT A18
SELECT   common_lookup_lab_id
,        common_lookup_context
,        common_lookup_type
FROM     common_lookup_lab;

-- --------------------------------------------------------
--  Step #7
--  -------
--  Query data against COMMON_LOOKUP_LAB
-- --------------------------------------------------------

SELECT common_lookup_lab_id
    FROM COMMON_LOOKUP_LAB
    WHERE common_lookup_context = UPPER('system_user_lab')
        AND common_lookup_type = UPPER('system_group');

SELECT common_lookup_lab_id
    FROM COMMON_LOOKUP_LAB
    WHERE common_lookup_context = UPPER('system_user_lab')
        AND common_lookup_type = UPPER('system_admin');

-- --------------------------------------------------------
--  Step #8
--  -------
--  *(WRONG INSTRUCTIONS)* !!!
--
--  Working with sub-queries
--
--  *(WRONG INSTRUCTIONS)* !!!
-- --------------------------------------------------------

-- Query #1
UPDATE SYSTEM_USER_LAB
    SET system_user_type =
        (SELECT common_lookup_lab_id
            FROM COMMON_LOOKUP_LAB
            WHERE common_lookup_context = UPPER('system_user_lab')
                AND common_lookup_type = UPPER('system_group'));

--- Testing Query #1
SET NULL '<Null>'
COL system_user_lab_id    FORMAT  9999
COL system_user_group_id  FORMAT  9999
COL system_user_type      FORMAT  9999
COL system_user_name      FORMAT  9999
SELECT   system_user_lab_id
,        system_user_name
,        system_user_group_id
,        system_user_type
FROM     system_user_lab
WHERE    system_user_lab_id = 1;

-- Query #2
UPDATE SYSTEM_USER_LAB
    SET system_user_group_id =
        (SELECT common_lookup_lab_id
            FROM COMMON_LOOKUP_LAB
            WHERE common_lookup_context = UPPER('system_user_lab')
                AND common_lookup_type = UPPER('system_admin'));

-- Testing Query #2
SET NULL '<Null>'
COL system_user_lab_id    FORMAT  9999
COL system_user_group_id  FORMAT  9999
COL system_user_type      FORMAT  9999
COL system_user_name      FORMAT  9999
SELECT   system_user_lab_id
,        system_user_name
,        system_user_group_id
,        system_user_type
FROM     system_user_lab
WHERE    system_user_lab_id = 1;


-- --------------------------------------------------------
--  Step #9
--  -------
--  *(WRONG INSTRUCTIONS)* !!!
--
--  Re-enable the already enabled FKs and the disabled ones
--
--  *(WRONG INSTRUCTIONS)* !!!
-- --------------------------------------------------------

-- Query #1
ALTER TABLE SYSTEM_USER_LAB ENABLE CONSTRAINT FK_SYSTEM_USER_LAB_3;
ALTER TABLE SYSTEM_USER_LAB ENABLE CONSTRAINT FK_SYSTEM_USER_LAB_4;

-- Test
COL table_name       FORMAT A14  HEADING "Table Name"
COL constraint_name  FORMAT A24  HEADING "Constraint Name"
COL constraint_type  FORMAT A15  HEADING "Constraint Type"
COL status           FORMAT A8   HEADING "Status"
SELECT   table_name
,        constraint_name
,        CASE
           WHEN constraint_type = 'R'        THEN 'FOREIGN KEY'
           WHEN constraint_type = 'P'        THEN 'PRIMARY KEY'
           WHEN constraint_type = 'U'        THEN 'UNIQUE'
           WHEN constraint_type = 'C' AND  REGEXP_LIKE(constraint_name,'^.*nn.*$','i')
           THEN 'NOT NULL'
           ELSE 'CHECK'
         END constraint_type
,        status
FROM     user_constraints
WHERE    table_name = 'SYSTEM_USER_LAB'
ORDER BY CASE
           WHEN constraint_type = 'PRIMARY KEY' THEN 1
           WHEN constraint_type = 'NOT NULL'    THEN 2
           WHEN constraint_type = 'CHECK'       THEN 3
           WHEN constraint_type = 'UNIQUE'      THEN 4
           WHEN constraint_type = 'FOREIGN KEY' THEN 5
         END
,        constraint_name;

-- Query #2
ALTER TABLE COMMON_LOOKUP_LAB ENABLE CONSTRAINT FK_CLOOKUP_LAB_1;
ALTER TABLE COMMON_LOOKUP_LAB ENABLE CONSTRAINT FK_CLOOKUP_LAB_2;

-- Test Query #2
COL table_name       FORMAT A14  HEADING "Table Name"
COL constraint_name  FORMAT A24  HEADING "Constraint Name"
COL constraint_type  FORMAT A15  HEADING "Constraint Type"
COL status           FORMAT A8   HEADING "Status"
SELECT   table_name
,        constraint_name
,        CASE
           WHEN constraint_type = 'R'        THEN 'FOREIGN KEY'
           WHEN constraint_type = 'P'        THEN 'PRIMARY KEY'
           WHEN constraint_type = 'U'        THEN 'UNIQUE'
           WHEN constraint_type = 'C' AND  REGEXP_LIKE(constraint_name,'^.*nn.*$','i')
           THEN 'NOT NULL'
           ELSE 'CHECK'
         END constraint_type
,        status
FROM     user_constraints
WHERE    table_name = 'COMMON_LOOKUP_LAB'
ORDER BY CASE
           WHEN constraint_type = 'PRIMARY KEY' THEN 1
           WHEN constraint_type = 'NOT NULL'    THEN 2
           WHEN constraint_type = 'CHECK'       THEN 3
           WHEN constraint_type = 'UNIQUE'      THEN 4
           WHEN constraint_type = 'FOREIGN KEY' THEN 5
         END
,        constraint_name;

-- --------------------------------------------------------
--  Step #10
--  -------
--  *(WRONG INSTRUCTIONS)* !!!
--  
--  Re-enable NOT NULL constraints on SYSTEM_USER_LAB
--
--  *(WRONG INSTRUCTIONS)* !!!
-- --------------------------------------------------------

-- Query
ALTER TABLE SYSTEM_USER_LAB
    MODIFY (system_user_group_id NUMBER CONSTRAINT nn_system_user_lab_2 NOT NULL);

ALTER TABLE SYSTEM_USER_LAB
    MODIFY (system_user_type NUMBER CONSTRAINT nn_system_user_lab_3 NOT NULL);

-- Testing
COL table_name       FORMAT A18  HEADING "Table Name"
COL constraint_name  FORMAT A28  HEADING "Constraint Name"
COL constraint_type  FORMAT A15  HEADING "Constraint Type"
COL status           FORMAT A8   HEADING "Status"
SELECT   table_name
,        constraint_name
,        CASE
           WHEN constraint_type = 'R'        THEN 'FOREIGN KEY'
           WHEN constraint_type = 'P'        THEN 'PRIMARY KEY'
           WHEN constraint_type = 'U'        THEN 'UNIQUE'
           WHEN constraint_type = 'C' AND  REGEXP_LIKE(constraint_name,'^.*nn.*$','i')
           THEN 'NOT NULL'
           ELSE 'CHECK'
         END constraint_type
,        status
FROM     user_constraints
WHERE    table_name = 'SYSTEM_USER_LAB'
ORDER BY CASE
           WHEN constraint_type = 'PRIMARY KEY' THEN 1
           WHEN constraint_type = 'NOT NULL'    THEN 2
           WHEN constraint_type = 'CHECK'       THEN 3
           WHEN constraint_type = 'UNIQUE'      THEN 4
           WHEN constraint_type = 'FOREIGN KEY' THEN 5
         END
,        constraint_name;


-- --------------------------------------------------------
--  Step #11
--  -------
--  Re-enable NOT NULL constraints on SYSTEM_USER_LAB
-- --------------------------------------------------------

-- Inserting "Phineas Phineas Phineas" - according to the instructions
-- found at http://michaelmclaughlin.info/db1/week-3-summary/lab-week-4/instructions/
--
-- Or more simply, adding SYSTEM_USER_LAB "Phineas Taylor Barnum"
INSERT INTO SYSTEM_USER_LAB (
    system_user_lab_id,
    system_user_type,
    system_user_group_id,
    system_user_name,
    first_name,
    middle_name,
    last_name,
    created_by,
    creation_date,
    last_updated_by,
    last_update_date
)
VALUES (
    system_user_lab_s1.nextVal,
    (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('system_group')),
    (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('dba')),
    'DBA1',
    'Phineas',
    'Taylor',
    'Barnum',
    (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
    SYSDATE,
    (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
    SYSDATE
);

-- Inserting "Phineas '' Phineas" - according to the instructions
-- found at http://michaelmclaughlin.info/db1/week-3-summary/lab-week-4/instructions/
--
-- Or more simply, adding SYSTEM_USER_LAB "Phineas Fogg"
INSERT INTO SYSTEM_USER_LAB (
    system_user_lab_id,
    system_user_type,
    system_user_group_id,
    system_user_name,
    first_name,
    middle_name,
    last_name,
    created_by,
    creation_date,
    last_updated_by,
    last_update_date
)
VALUES (
    system_user_lab_s1.nextVal,
    (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('system_group')),
    (SELECT common_lookup_lab_id FROM COMMON_LOOKUP_LAB WHERE common_lookup_context = UPPER('system_user_lab') AND common_lookup_type = UPPER('dba')),
    'DBA2',
    'Phineas',
    '',
    'Fogg',
    (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
    SYSDATE,
    (SELECT system_user_lab_id FROM SYSTEM_USER_LAB WHERE system_user_name = UPPER('sysadmin')),
    SYSDATE
);

-- --------------------------------------------------------
--  Step #12
--  -------
--  Test for Step #11
-- --------------------------------------------------------
SET NULL '<Null>'
COL system_user_id        FORMAT  9999
COL system_user_group_id  FORMAT  9999
COL system_user_type      FORMAT  9999
COL system_user_name      FORMAT  A10
COL full_user_name        FORMAT  A30
SELECT   system_user_lab_id
,        system_user_group_id
,        system_user_type
,        system_user_name
,        CASE
           WHEN last_name IS NOT NULL THEN
             last_name || ', ' || first_name || ' ' || middle_name
         END AS full_user_name
FROM     system_user_lab;

-- --------------------------------------------------------
--  Step Final
--  -------
--  Commit Changes
-- --------------------------------------------------------
COMMIT;

SPOOL OFF