-- ------------------------------------------------------------------
--  Program Name:   apply_oracle_lab9.sql
--  Lab Assignment: Lab #9
--  Program Author: Michael McLaughlin
--  Creation Date:  02-Mar-2010
-- ------------------------------------------------------------------
-- Instructions:
-- ------------------------------------------------------------------
-- The two scripts contain spooling commands, which is why there
-- isn't a spooling command in this script. When you run this file
-- you first connect to the Oracle database with this syntax:
--
--   sqlplus student/student@xe
--
-- Then, you call this script with the following syntax:
--
--   sql> @apply_oracle_lab9.sql
--
-- ------------------------------------------------------------------

-- Run the prior lab script.
@@/home/student/Data/cit225/oracle/lab8/apply_oracle_lab8.sql

DROP SEQUENCE TRANSACTION_S1;
DROP SEQUENCE AIRPORT_S1;
DROP SEQUENCE ACCOUNT_LIST_S1;
 
-- Open log file.  
SPOOL apply_oracle_lab9.txt

-- --------------------------------------------------------
--  Step #1
--  -------
--  Create the TRANSACTION table per the web page spec.
-- --------------------------------------------------------

-- Step a)
-- Create table.
CREATE TABLE TRANSACTION
( TRANSACTION_ID 	        NUMBER          NOT NULL
, TRANSACTION_ACCOUNT       VARCHAR2(15)   NOT NULL
, TRANSACTION_TYPE 	        NUMBER          NOT NULL
, TRANSACTION_DATE 	        DATE            NOT NULL
, TRANSACTION_AMOUNT 	    NUMBER          NOT NULL
, RENTAL_ID 	     	    NUMBER          NOT NULL
, PAYMENT_METHOD_TYPE       NUMBER          NOT NULL
, PAYMENT_ACCOUNT_NUMBER 	VARCHAR2(19)   NOT NULL
, CREATED_BY 	            NUMBER          NOT NULL
, CREATION_DATE 	        DATE            NOT NULL
, LAST_UPDATED_BY 	        NUMBER          NOT NULL
, LAST_UPDATE_DATE 	        DATE            NOT NULL
, CONSTRAINT PK_TRANSACTION_1   PRIMARY KEY(TRANSACTION_ID)
, CONSTRAINT FK_TRANSACTION_1   FOREIGN KEY(TRANSACTION_TYPE) REFERENCES COMMON_LOOKUP(COMMON_LOOKUP_ID)
, CONSTRAINT FK_TRANSACTION_2   FOREIGN KEY(RENTAL_ID) REFERENCES RENTAL(RENTAL_ID)
, CONSTRAINT FK_TRANSACTION_3   FOREIGN KEY(PAYMENT_METHOD_TYPE) REFERENCES COMMON_LOOKUP(COMMON_LOOKUP_ID)
, CONSTRAINT FK_TRANSACTION_4   FOREIGN KEY(CREATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID)
, CONSTRAINT FK_TRANSACTION_5   FOREIGN KEY(LAST_UPDATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID));

-- Create a sequence.
CREATE SEQUENCE TRANSACTION_S1 START WITH 1 NOCACHE;

-- Test
COLUMN table_name   FORMAT A14  HEADING "Table Name"
COLUMN column_id    FORMAT 9999 HEADING "Column ID"
COLUMN column_name  FORMAT A22  HEADING "Column Name"
COLUMN nullable     FORMAT A8   HEADING "Nullable"
COLUMN data_type    FORMAT A12  HEADING "Data Type"
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'TRANSACTION'
ORDER BY 2;

-- Step b)
-- Create a unique natural index key.
CREATE UNIQUE INDEX NATURAL_KEY ON TRANSACTION(
    RENTAL_ID, 
    TRANSACTION_TYPE, 
    TRANSACTION_DATE, 
    PAYMENT_METHOD_TYPE, 
    PAYMENT_ACCOUNT_NUMBER,
    TRANSACTION_ACCOUNT
);

-- Test

COLUMN table_name       FORMAT A12  HEADING "Table Name"
COLUMN index_name       FORMAT A16  HEADING "Index Name"
COLUMN uniqueness       FORMAT A8   HEADING "Unique"
COLUMN column_position  FORMAT 9999 HEADING "Column Position"
COLUMN column_name      FORMAT A24  HEADING "Column Name"
SELECT   i.table_name
,        i.index_name
,        i.uniqueness
,        ic.column_position
,        ic.column_name
FROM     user_indexes i INNER JOIN user_ind_columns ic
ON       i.index_name = ic.index_name
WHERE    i.table_name = 'TRANSACTION'
AND      i.uniqueness = 'UNIQUE'
AND      i.index_name = 'NATURAL_KEY';
 
-- --------------------------------------------------------
--  Step #2
--  -------
--  Insert two rows for the TRANSACTION_TYPE column and
--  four rows for the PAYMENT_METHOD_TYPE column of the
--  TRANSACTION table into the COMMON_LOOKUP table.
-- --------------------------------------------------------

INSERT INTO COMMON_LOOKUP VALUES
    (COMMON_LOOKUP_S1.nextval, 'CREDIT', 'Credit', 1, SYSDATE, 1, SYSDATE, 'TRANSACTION', 'TRANSACTION_TYPE', 'CR');
 
INSERT INTO COMMON_LOOKUP VALUES
    (COMMON_LOOKUP_S1.nextval, 'DEBIT', 'Debit', 1, SYSDATE, 1, SYSDATE, 'TRANSACTION', 'TRANSACTION_TYPE', 'DR');
 
INSERT INTO COMMON_LOOKUP VALUES
    (COMMON_LOOKUP_S1.nextval, 'DISCOVER_CARD', 'Discover Card', 1, SYSDATE, 1, SYSDATE, 'TRANSACTION', 'PAYMENT_METHOD_TYPE', '');
 
INSERT INTO COMMON_LOOKUP VALUES
    (COMMON_LOOKUP_S1.nextval, 'VISA_CARD', 'Visa Card', 1, SYSDATE, 1, SYSDATE, 'TRANSACTION', 'PAYMENT_METHOD_TYPE', '');
 
INSERT INTO COMMON_LOOKUP VALUES
    (COMMON_LOOKUP_S1.nextval, 'MASTER_CARD', 'Master Card', 1, SYSDATE, 1, SYSDATE, 'TRANSACTION', 'PAYMENT_METHOD_TYPE', '');

INSERT INTO COMMON_LOOKUP VALUES
    (COMMON_LOOKUP_S1.nextval, 'CASH', 'Cash', 1, SYSDATE, 1, SYSDATE, 'TRANSACTION', 'PAYMENT_METHOD_TYPE', '');

-- Test

COLUMN common_lookup_table  FORMAT A20 HEADING "COMMON_LOOKUP_TABLE"
COLUMN common_lookup_column FORMAT A20 HEADING "COMMON_LOOKUP_COLUMN"
COLUMN common_lookup_type   FORMAT A20 HEADING "COMMON_LOOKUP_TYPE"
SELECT   common_lookup_table
,        common_lookup_column
,        common_lookup_type
FROM     common_lookup
WHERE    common_lookup_table = 'TRANSACTION'
AND      common_lookup_column IN ('TRANSACTION_TYPE','PAYMENT_METHOD_TYPE')
ORDER BY 1, 2, 3 DESC;

-- --------------------------------------------------------
--  Step #3
--  -------
--  Create the AIRPORT and ACCOUNT_LIST tables; and 
--  insert rows into both tables.
-- --------------------------------------------------------
 
-- Step a)
-- Create AIRPORT table.
CREATE TABLE AIRPORT
( AIRPORT_ID                NUMBER          NOT NULL
, AIRPORT_CODE              VARCHAR2(3)     NOT NULL
, AIRPORT_CITY              VARCHAR2(30)    NOT NULL
, CITY                      VARCHAR2(30)    NOT NULL
, STATE_PROVINCE            VARCHAR2(30)    NOT NULL
, CREATED_BY 	            NUMBER          NOT NULL
, CREATION_DATE 	        DATE            NOT NULL
, LAST_UPDATED_BY 	        NUMBER          NOT NULL
, LAST_UPDATE_DATE 	        DATE            NOT NULL
, CONSTRAINT PK_AIRPORT_1   PRIMARY KEY(AIRPORT_ID)
, CONSTRAINT FK_AIRPORT_1   FOREIGN KEY(CREATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID)
, CONSTRAINT FK_AIRPORT_2   FOREIGN KEY(LAST_UPDATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID));

-- Create AIRPORT sequence.
CREATE SEQUENCE AIRPORT_S1 START WITH 1 NOCACHE;

-- Test
COLUMN table_name   FORMAT A14  HEADING "Table Name"
COLUMN column_id    FORMAT 9999 HEADING "Column ID"
COLUMN column_name  FORMAT A22  HEADING "Column Name"
COLUMN nullable     FORMAT A8   HEADING "Nullable"
COLUMN data_type    FORMAT A12  HEADING "Data Type"
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'AIRPORT'
ORDER BY 2;

-- Step b)
-- Create a unique NK_AIRPORT key.
CREATE UNIQUE INDEX NK_AIRPORT ON AIRPORT(
    AIRPORT_CODE,
    AIRPORT_CITY,
    CITY,
    STATE_PROVINCE
);

-- Test
COLUMN table_name       FORMAT A12  HEADING "Table Name"
COLUMN index_name       FORMAT A16  HEADING "Index Name"
COLUMN uniqueness       FORMAT A8   HEADING "Unique"
COLUMN column_position  FORMAT 9999 HEADING "Column Position"
COLUMN column_name      FORMAT A24  HEADING "Column Name"
SELECT   i.table_name
,        i.index_name
,        i.uniqueness
,        ic.column_position
,        ic.column_name
FROM     user_indexes i INNER JOIN user_ind_columns ic
ON       i.index_name = ic.index_name
WHERE    i.table_name = 'AIRPORT'
AND      i.uniqueness = 'UNIQUE'
AND      i.index_name = 'NK_AIRPORT';

-- Step c)
-- Seed AIRPORT table

INSERT INTO AIRPORT VALUES
    (AIRPORT_S1.nextval, 'LAX', 'Los Angeles', 'Los Angeles', 'California', 1, SYSDATE, 1, SYSDATE);

INSERT INTO AIRPORT VALUES
    (AIRPORT_S1.nextval, 'SLC', 'Salt Lake City', 'Provo', 'Utah', 1, SYSDATE, 1, SYSDATE);

INSERT INTO AIRPORT VALUES
    (AIRPORT_S1.nextval, 'SLC', 'Salt Lake City', 'Spanish Fork', 'Utah', 1, SYSDATE, 1, SYSDATE);

INSERT INTO AIRPORT VALUES
    (AIRPORT_S1.nextval, 'SFO', 'San Francisco', 'San Francsico', 'California', 1, SYSDATE, 1, SYSDATE);

INSERT INTO AIRPORT VALUES
    (AIRPORT_S1.nextval, 'SJC', 'San Jose', 'San Jose', 'California', 1, SYSDATE, 1, SYSDATE);

INSERT INTO AIRPORT VALUES
    (AIRPORT_S1.nextval, 'SJC', 'San Jose', 'San Carlos', 'California', 1, SYSDATE, 1, SYSDATE);

-- Test
COLUMN code           FORMAT A4  HEADING "Code"
COLUMN airport_city   FORMAT A14 HEADING "Airport City"
COLUMN city           FORMAT A14 HEADING "City"
COLUMN state_province FORMAT A10 HEADING "State or|Province"
SELECT   airport_code AS code
,        airport_city
,        city
,        state_province
FROM     airport;

-- Step d)
-- Create ACCOUNT_LIST table.
CREATE TABLE ACCOUNT_LIST
( ACCOUNT_LIST_ID           NUMBER          NOT NULL
, ACCOUNT_NUMBER            VARCHAR2(10)    NOT NULL
, CONSUMED_DATE             DATE            
, CONSUMED_BY               NUMBER          
, CREATED_BY 	            NUMBER          NOT NULL
, CREATION_DATE 	        DATE            NOT NULL
, LAST_UPDATED_BY 	        NUMBER          NOT NULL
, LAST_UPDATE_DATE 	        DATE            NOT NULL
, CONSTRAINT PK_ACCOUNT_LIST_1   PRIMARY KEY(ACCOUNT_LIST_ID)
, CONSTRAINT FK_ACCOUNT_LIST_1   FOREIGN KEY(CONSUMED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID)
, CONSTRAINT FK_ACCOUNT_LIST_2   FOREIGN KEY(CREATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID)
, CONSTRAINT FK_ACCOUNT_LIST_3   FOREIGN KEY(LAST_UPDATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID));

-- Create ACCOUNT_LIST sequence.
CREATE SEQUENCE ACCOUNT_LIST_S1 START WITH 1 NOCACHE;

-- Test
COLUMN table_name   FORMAT A14
COLUMN column_id    FORMAT 9999
COLUMN column_name  FORMAT A22
COLUMN data_type    FORMAT A12
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'ACCOUNT_LIST'
ORDER BY 2;

-- Step e)
-- Seed ACCOUNT_LIST with provided data
@@/home/student/Data/cit225/oracle/lab9/account_list_seed.sql

-- Test
COLUMN object_name FORMAT A18
COLUMN object_type FORMAT A12
SELECT   object_name
,        object_type
FROM     user_objects
WHERE    object_name = 'SEED_ACCOUNT_LIST';

EXECUTE seed_account_list();

-- Test
COLUMN airport FORMAT A7
SELECT   SUBSTR(account_number,1,3) AS "Airport"
,        COUNT(*) AS "# Accounts"
FROM     account_list
WHERE    consumed_date IS NULL
GROUP BY SUBSTR(account_number,1,3)
ORDER BY 1;

-- Step f)
-- Update US Postal Codes
UPDATE address
SET    state_province = 'California'
WHERE  state_province = 'CA';

-- Step g)
-- Update MEMBER accounts
@@/home/student/Data/cit225/oracle/lab9/update_member_account.sql

-- Test
COLUMN object_name FORMAT A22
COLUMN object_type FORMAT A12
SELECT   object_name
,        object_type
FROM     user_objects
WHERE    object_name = 'UPDATE_MEMBER_ACCOUNT';

-- Execute
EXECUTE update_member_account();
-- Test
-- Format the SQL statement display.
COLUMN member_id      FORMAT 999999 HEADING "Member|ID #"
COLUMN last_name      FORMAT A7     HEADING "Last|Name"
COLUMN account_number FORMAT A10    HEADING "Account|Number"
COLUMN acity          FORMAT A12    HEADING "Address City"
COLUMN apstate        FORMAT A10    HEADING "Airport|State or|Province"
COLUMN alcode         FORMAT A5     HEADING "Airport|Account|Code"
 
-- Query distinct members and addresses.
SELECT   DISTINCT
         m.member_id
,        c.last_name
,        m.account_number
,        a.city AS acity
,        ap.state_province AS apstate
,        SUBSTR(al.account_number,1,3) AS alcode
FROM     member m INNER JOIN contact c
ON       m.member_id = c.member_id INNER JOIN address a
ON       c.contact_id = a.contact_id INNER JOIN airport ap
ON       a.city = ap.city
AND      a.state_province = ap.state_province INNER JOIN account_list al
ON       ap.airport_code = SUBSTR(al.account_number,1,3)
ORDER BY 1;


-- --------------------------------------------------------
--  Step #4
--  -------
--  Create an external table TRANSACTION_UPLOAD that uses
--  a pre-seeded source file.
-- --------------------------------------------------------

CREATE TABLE TRANSACTION_UPLOAD
(
    ACCOUNT_NUMBER VARCHAR2(10),
    FIRST_NAME VARCHAR2(20),
    MIDDLE_NAME VARCHAR2(20),
    LAST_NAME VARCHAR2(20),
    CHECK_OUT_DATE DATE,
    RETURN_DATE DATE,
    RENTAL_ITEM_TYPE VARCHAR2(12),
    TRANSACTION_TYPE VARCHAR2(14),
    TRANSACTION_AMOUNT NUMBER,
    TRANSACTION_DATE DATE,
    ITEM_ID NUMBER,
    PAYMENT_METHOD_TYPE VARCHAR2(14),
    PAYMENT_ACCOUNT_NUMBER VARCHAR2(19)
)
ORGANIZATION EXTERNAL
(
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY UPLOAD
    ACCESS PARAMETERS
    (
        RECORDS DELIMITED BY NEWLINE CHARACTERSET US7ASCII
        BADFILE     'UPLOAD':'transaction_upload.bad'
        DISCARDFILE 'UPLOAD':'transaction_upload.dis'
        LOGFILE     'UPLOAD':'transaction_upload.log'
        FIELDS TERMINATED BY ','
        OPTIONALLY ENCLOSED BY "'"
        MISSING FIELD VALUES ARE NULL
    )
    LOCATION ('transaction_upload.csv')
)
REJECT LIMIT UNLIMITED;

-- Test
SET LONG 200000  -- Enables the display of the full statement.
SELECT   dbms_metadata.get_ddl('TABLE','TRANSACTION_UPLOAD') AS "Table Description"
FROM     dual;

-- Count External Table Rows
SELECT   COUNT(*) AS "External Rows"
FROM     transaction_upload;
 
-- Close log file.
SPOOL OFF
 
-- Make all changes permanent.
COMMIT;