-- ------------------------------------------------------------------
--  Program Name:   apply_oracle_lab6.sql
--  Lab Assignment: Lab #6
--  Program Author: Michael McLaughlin
--  Creation Date:  02-Mar-2018
-- ------------------------------------------------------------------
-- Instructions:
-- ------------------------------------------------------------------
-- The two scripts contain spooling commands, which is why there
-- isn't a spooling command in this script. When you run this file
-- you first connect to the Oracle database with this syntax:
--
--   sqlplus student/student@xe
--
-- Then, you call this script with the following syntax:
--
--   sql> @apply_oracle_lab6.sql
--
-- ------------------------------------------------------------------

-- Call library files.
@/home/student/Data/cit225/oracle/lab5/apply_oracle_lab5.sql

-- Open log file.
SPOOL apply_oracle_lab6.txt

-- --------------------------------------------------------
--  Step #1
--  -------
--  Alter RENTAL_ITEM Table
-- --------------------------------------------------------
ALTER TABLE RENTAL_ITEM
ADD 
    (
        RENTAL_ITEM_PRICE NUMBER, 
        RENTAL_ITEM_TYPE NUMBER
    );

ALTER TABLE RENTAL_ITEM
ADD
    (
        CONSTRAINT FK_RENTAL_ITEM_5 FOREIGN KEY(RENTAL_ITEM_TYPE) REFERENCES COMMON_LOOKUP(COMMON_LOOKUP_ID)
    );

-- Test Query
SET NULL ''
COLUMN table_name   FORMAT A14
COLUMN column_id    FORMAT 9999
COLUMN column_name  FORMAT A22
COLUMN data_type    FORMAT A12
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'RENTAL_ITEM'
ORDER BY 2;

-- --------------------------------------------------------
--  Step #2
--  -------
--  Create PRICE table
-- --------------------------------------------------------
CREATE TABLE PRICE (
     PRICE_ID            INTEGER     CONSTRAINT PK_PRICE PRIMARY KEY   NOT NULL
,    ITEM_ID             INTEGER     CONSTRAINT NN_PRICE_1             NOT NULL
,    PRICE_TYPE          INTEGER                                       NULL    
,    ACTIVE_FLAG         VARCHAR2(1)                                   NOT NULL
,    START_DATE          DATE        CONSTRAINT NN_PRICE_3             NOT NULL
,    END_DATE            DATE                                          NULL    
,    AMOUNT              INTEGER     CONSTRAINT NN_PRICE_4             NOT NULL
,    CREATED_BY          INTEGER     CONSTRAINT NN_PRICE_5             NOT NULL
,    CREATION_DATE       DATE        CONSTRAINT NN_PRICE_6             NOT NULL
,    LAST_UPDATED_BY     INTEGER     CONSTRAINT NN_PRICE_7             NOT NULL
,    LAST_UPDATE_DATE    DATE        CONSTRAINT NN_PRICE_8             NOT NULL
,    CONSTRAINT YN_PRICE CHECK(ACTIVE_FLAG IN ('Y','N'))
,    CONSTRAINT FK_PRICE_1 FOREIGN KEY(ITEM_ID) REFERENCES ITEM(ITEM_ID)
,    CONSTRAINT FK_PRICE_2 FOREIGN KEY(PRICE_TYPE) REFERENCES COMMON_LOOKUP(COMMON_LOOKUP_ID)
,    CONSTRAINT FK_PRICE_3 FOREIGN KEY(CREATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID)
,    CONSTRAINT FK_PRICE_4 FOREIGN KEY(LAST_UPDATED_BY) REFERENCES SYSTEM_USER(SYSTEM_USER_ID)
);

-- Test Query
SET NULL ''
COLUMN table_name   FORMAT A14
COLUMN column_id    FORMAT 9999
COLUMN column_name  FORMAT A22
COLUMN data_type    FORMAT A12
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'PRICE'
ORDER BY 2;

-- Test Constraint
COLUMN constraint_name   FORMAT A16
COLUMN search_condition  FORMAT A30
SELECT   uc.constraint_name
,        uc.search_condition
FROM     user_constraints uc INNER JOIN user_cons_columns ucc
ON       uc.table_name = ucc.table_name
AND      uc.constraint_name = ucc.constraint_name
WHERE    uc.table_name = UPPER('price')
AND      ucc.column_name = UPPER('active_flag')
AND      uc.constraint_name = UPPER('yn_price')
AND      uc.constraint_type = 'C';

-- --------------------------------------------------------
--  Step #3 - Part #1
--  -------
--  Rename the ITEM_RELEASE_DATE column of the ITEM table to RELEASE_DATE
-- --------------------------------------------------------
ALTER TABLE ITEM
    RENAME COLUMN ITEM_RELEASE_DATE TO RELEASE_DATE;

-- Test Query
SET NULL ''
COLUMN table_name   FORMAT A14
COLUMN column_id    FORMAT 9999
COLUMN column_name  FORMAT A22
COLUMN data_type    FORMAT A12
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'ITEM'
ORDER BY 2;

-- --------------------------------------------------------
--  Step #3 - Part #2
--  -------
--  Insert three new DVD releases into the ITEM table
-- --------------------------------------------------------
INSERT INTO ITEM
    VALUES (item_s1.nextval, 0705632085944, 1, 'Tron', '', 'PG-13', (TRUNC(SYSDATE) - 1), 1, SYSDATE, 1, SYSDATE);
INSERT INTO ITEM
    VALUES (item_s1.nextval, 0705632085945, 1, 'Ender''s Game', '', 'PG-13', (TRUNC(SYSDATE) - 1), 1, SYSDATE, 1, SYSDATE);
INSERT INTO ITEM
    VALUES (item_s1.nextval, 0705632085946, 1, 'Elysium', '', 'PG-13', (TRUNC(SYSDATE) - 1), 1, SYSDATE, 1, SYSDATE);

-- Test Query
SELECT   i.item_title
,        SYSDATE AS today
,        i.release_date
FROM     item i
WHERE   (SYSDATE - i.release_date) < 31;

-- --------------------------------------------------------
--  Step #3 - Part #3
--  -------
--  Insert a new member account with three contacts
-- --------------------------------------------------------
INSERT INTO MEMBER
    VALUES (member_s1.nextval, 
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MEMBER' AND COMMON_LOOKUP_TYPE = 'GROUP'),
    'US00011',
    '6011 0000 0000 0078',
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MEMBER' AND COMMON_LOOKUP_TYPE = 'DISCOVER_CARD'),
    1,
    SYSDATE,
    1,
    SYSDATE);

-- Insert Harry Potter
INSERT INTO CONTACT
    VALUES (contact_s1.nextval,
    member_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'CONTACT' AND COMMON_LOOKUP_TYPE = 'CUSTOMER'),
    'Harry',
    '',
    'Potter',
    1,
    SYSDATE,
    1,
    SYSDATE);

INSERT INTO ADDRESS
    VALUES (address_s1.nextval,
    contact_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MULTIPLE' AND COMMON_LOOKUP_TYPE = 'HOME'),
    'Provo',
    'Utah',
    '84604',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

INSERT INTO STREET_ADDRESS
    VALUES (street_address_s1.nextval,
    address_s1.currval,
    '900 E, 300 N',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

INSERT INTO TELEPHONE
    VALUES (telephone_s1.nextval,
    contact_s1.currval,
    address_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MULTIPLE' AND COMMON_LOOKUP_TYPE = 'HOME'),
    '1',
    '801',
    '333-3333',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

-- Insert Ginny Potter
INSERT INTO CONTACT
    VALUES (contact_s1.nextval,
    member_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'CONTACT' AND COMMON_LOOKUP_TYPE = 'CUSTOMER'),
    'Ginny',
    '',
    'Potter',
    1,
    SYSDATE,
    1,
    SYSDATE);

INSERT INTO ADDRESS
    VALUES (address_s1.nextval,
    contact_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MULTIPLE' AND COMMON_LOOKUP_TYPE = 'HOME'),
    'Provo',
    'Utah',
    '84604',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

INSERT INTO STREET_ADDRESS
    VALUES (street_address_s1.nextval,
    address_s1.currval,
    '900 E, 300 N',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

INSERT INTO TELEPHONE
    VALUES (telephone_s1.nextval,
    contact_s1.currval,
    address_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MULTIPLE' AND COMMON_LOOKUP_TYPE = 'HOME'),
    '1',
    '801',
    '333-3333',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

-- Insert Lily Luna Potter
INSERT INTO CONTACT
    VALUES (contact_s1.nextval,
    member_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'CONTACT' AND COMMON_LOOKUP_TYPE = 'CUSTOMER'),
    'Lily',
    'Luna',
    'Potter',
    1,
    SYSDATE,
    1,
    SYSDATE);

INSERT INTO ADDRESS
    VALUES (address_s1.nextval,
    contact_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MULTIPLE' AND COMMON_LOOKUP_TYPE = 'HOME'),
    'Provo',
    'Utah',
    '84604',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

INSERT INTO STREET_ADDRESS
    VALUES (street_address_s1.nextval,
    address_s1.currval,
    '900 E, 300 N',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

INSERT INTO TELEPHONE
    VALUES (telephone_s1.nextval,
    contact_s1.currval,
    address_s1.currval,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'MULTIPLE' AND COMMON_LOOKUP_TYPE = 'HOME'),
    '1',
    '801',
    '333-3333',
    1,
    SYSDATE,
    1,
    SYSDATE
    );

-- Test Query
COLUMN account_number  FORMAT A10  HEADING "Account|Number"
COLUMN full_name       FORMAT A16  HEADING "Name|(Last, First MI)"
COLUMN street_address  FORMAT A14  HEADING "Street Address"
COLUMN city            FORMAT A10  HEADING "City"
COLUMN state           FORMAT A10  HEADING "State"
COLUMN postal_code     FORMAT A6   HEADING "Postal|Code"
SELECT   m.account_number
,        c.last_name || ', ' || c.first_name
||       CASE
           WHEN c.middle_name IS NOT NULL THEN
             ' ' || c.middle_name || ' '
         END AS full_name
,        sa.street_address
,        a.city
,        a.state_province AS state
,        a.postal_code
FROM     member m INNER JOIN contact c
ON       m.member_id = c.member_id INNER JOIN address a
ON       c.contact_id = a.contact_id INNER JOIN street_address sa
ON       a.address_id = sa.address_id INNER JOIN telephone t
ON       c.contact_id = t.contact_id
WHERE    c.last_name = 'Potter';

-- --------------------------------------------------------
--  Step #3 - Part #4
--  -------
--  Insert two rows into the RENTAL table with a dependent
--  row for each in the RENTAL_ITEM table; and one row into
--  the RENTAL table with two dependent rows in the RENTAL_ITEM table
-- --------------------------------------------------------

-- New entry for Harry Potter's rentals
INSERT INTO RENTAL
    VALUES (rental_s1.nextval,
    (SELECT CONTACT_ID FROM CONTACT WHERE FIRST_NAME LIKE 'Harry' AND LAST_NAME LIKE 'Potter'),
    TRUNC(SYSDATE),
    TRUNC(SYSDATE+1),
    1,
    SYSDATE,
    1,
    SYSDATE);
-- Harry Potter's rental item
INSERT INTO RENTAL_ITEM
    VALUES (rental_item_s1.nextval,
    rental_s1.currval,
    (SELECT ITEM_ID FROM ITEM WHERE (SYSDATE - ITEM.RELEASE_DATE) < 31 AND ROWNUM = 1),
    1,
    SYSDATE,
    1,
    SYSDATE,
    10,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'ITEM' AND COMMON_LOOKUP_TYPE = 'BLU-RAY')
    );

INSERT INTO RENTAL_ITEM
    VALUES (rental_item_s1.nextval,
    rental_s1.currval,
    (SELECT ITEM_ID FROM ITEM WHERE (SYSDATE - ITEM.RELEASE_DATE) > 360 AND ROWNUM = 1),
    1,
    SYSDATE,
    1,
    SYSDATE,
    10,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'ITEM' AND COMMON_LOOKUP_TYPE = 'BLU-RAY')
    );

-- New entry for Ginny Potter's rentals
INSERT INTO RENTAL
    VALUES (rental_s1.nextval,
    (SELECT CONTACT_ID FROM CONTACT WHERE FIRST_NAME LIKE 'Ginny' AND LAST_NAME LIKE 'Potter'),
    TRUNC(SYSDATE),
    TRUNC(SYSDATE+3),
    1,
    SYSDATE,
    1,
    SYSDATE);
-- Ginny Potter's rental item
INSERT INTO RENTAL_ITEM
    VALUES (rental_item_s1.nextval,
    rental_s1.currval,
    (SELECT ITEM_ID FROM ITEM WHERE (SYSDATE - ITEM.RELEASE_DATE) < 31 AND ROWNUM = 1),
    1,
    SYSDATE,
    1,
    SYSDATE,
    10,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'ITEM' AND COMMON_LOOKUP_TYPE = 'BLU-RAY')
    );

-- New entry for Lily Luna Potter's rentals
INSERT INTO RENTAL
    VALUES (rental_s1.nextval,
    (SELECT CONTACT_ID FROM CONTACT WHERE FIRST_NAME LIKE 'Lily' AND LAST_NAME LIKE 'Potter'),
    TRUNC(SYSDATE),
    TRUNC(SYSDATE+5),
    1,
    SYSDATE,
    1,
    SYSDATE);
-- Lily Luna Potter's rental item
INSERT INTO RENTAL_ITEM
    VALUES (rental_item_s1.nextval,
    rental_s1.currval,
    (SELECT ITEM_ID FROM ITEM WHERE (SYSDATE - ITEM.RELEASE_DATE) < 31 AND ROWNUM = 1),
    1,
    SYSDATE,
    1,
    SYSDATE,
    10,
    (SELECT COMMON_LOOKUP_ID FROM COMMON_LOOKUP WHERE COMMON_LOOKUP_CONTEXT = 'ITEM' AND COMMON_LOOKUP_TYPE = 'BLU-RAY')
    );

-- Test Query
COLUMN full_name   FORMAT A18
COLUMN rental_id   FORMAT 9999
COLUMN rental_days FORMAT A14
COLUMN rentals     FORMAT 9999
COLUMN items       FORMAT 9999
SELECT   c.last_name||', '||c.first_name||' '||c.middle_name AS full_name
,        r.rental_id
,       (r.return_date - r.check_out_date) || '-DAY RENTAL' AS rental_days
,        COUNT(DISTINCT r.rental_id) AS rentals
,        COUNT(ri.rental_item_id) AS items
FROM     rental r INNER JOIN rental_item ri
ON       r.rental_id = ri.rental_id INNER JOIN contact c
ON       r.customer_id = c.contact_id
WHERE   (SYSDATE - r.check_out_date) < 15
AND      c.last_name = 'Potter'
GROUP BY c.last_name||', '||c.first_name||' '||c.middle_name
,        r.rental_id
,       (r.return_date - r.check_out_date) || '-DAY RENTAL'
ORDER BY 2;

-- --------------------------------------------------------
--  Step #4 - Part #1
--  -------
--  Drop the COMMON_LOOKUP_N1 and COMMON_LOOKUP_U2 indexes
-- --------------------------------------------------------

DROP INDEX COMMON_LOOKUP_N1;
DROP INDEX COMMON_LOOKUP_U2;

-- Test Query
COLUMN table_name FORMAT A14
COLUMN index_name FORMAT A20
SELECT   table_name
,        index_name
FROM     user_indexes
WHERE    table_name = 'COMMON_LOOKUP';

-- --------------------------------------------------------
--  Step #4 - Part #2-3
--  -------
--  Add three new columns to the COMMON_LOOKUP table.
-- --------------------------------------------------------

ALTER TABLE COMMON_LOOKUP
ADD 
    (
        COMMON_LOOKUP_TABLE VARCHAR2(30),
        COMMON_LOOKUP_COLUMN VARCHAR2(30),
        COMMON_LOOKUP_CODE VARCHAR2(30)
    );

-- Test Query
    SET NULL ''
    COLUMN table_name   FORMAT A14
    COLUMN column_id    FORMAT 9999
    COLUMN column_name  FORMAT A22
    COLUMN data_type    FORMAT A12
    SELECT   table_name
    ,        column_id
    ,        column_name
    ,        CASE
            WHEN nullable = 'N' THEN 'NOT NULL'
            ELSE ''
            END AS nullable
    ,        CASE
            WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
                data_type||'('||data_length||')'
            ELSE
                data_type
            END AS data_type
    FROM     user_tab_columns
    WHERE    table_name = 'COMMON_LOOKUP'
    ORDER BY 2;

-- Verify before updates
    COLUMN common_lookup_context  FORMAT A14  HEADING "Common|Lookup Context"
    COLUMN common_lookup_table    FORMAT A12  HEADING "Common|Lookup Table"
    COLUMN common_lookup_column   FORMAT A18  HEADING "Common|Lookup Column"
    COLUMN common_lookup_type     FORMAT A18  HEADING "Common|Lookup Type"
    SELECT   common_lookup_context
    ,        common_lookup_table
    ,        common_lookup_column
    ,        common_lookup_type
    FROM     common_lookup
    ORDER BY 1, 2, 3;

-- Migrate the COMMON_LOOKUP_CONTEXT column values
UPDATE COMMON_LOOKUP
    SET    COMMON_LOOKUP_TABLE = COMMON_LOOKUP_CONTEXT
    WHERE  COMMON_LOOKUP_CONTEXT != 'MULTIPLE';

-- Verify
    COLUMN common_lookup_context  FORMAT A14  HEADING "Common|Lookup Context"
    COLUMN common_lookup_table    FORMAT A12  HEADING "Common|Lookup Table"
    COLUMN common_lookup_column   FORMAT A18  HEADING "Common|Lookup Column"
    COLUMN common_lookup_type     FORMAT A18  HEADING "Common|Lookup Type"
    SELECT   common_lookup_context
    ,        common_lookup_table
    ,        common_lookup_column
    ,        common_lookup_type
    FROM     common_lookup
    ORDER BY 1, 2, 3;

-- Update the COMMON_LOOKUP_TABLE column with the value of of 'ADDRESS'
UPDATE COMMON_LOOKUP
    SET    COMMON_LOOKUP_TABLE = 'ADDRESS'
    WHERE  COMMON_LOOKUP_CONTEXT = 'MULTIPLE';

-- Verify
    COLUMN common_lookup_context  FORMAT A14  HEADING "Common|Lookup Context"
    COLUMN common_lookup_table    FORMAT A12  HEADING "Common|Lookup Table"
    COLUMN common_lookup_column   FORMAT A18  HEADING "Common|Lookup Column"
    COLUMN common_lookup_type     FORMAT A18  HEADING "Common|Lookup Type"
    SELECT   common_lookup_context
    ,        common_lookup_table
    ,        common_lookup_column
    ,        common_lookup_type
    FROM     common_lookup
    ORDER BY 1, 2, 3;

-- Create COMMON_LOOKUP_COLUMN column values 
UPDATE COMMON_LOOKUP
    SET    COMMON_LOOKUP_COLUMN = COMMON_LOOKUP_CONTEXT||'_TYPE'
    WHERE  COMMON_LOOKUP_CONTEXT IN (
        SELECT TABLE_NAME FROM ALL_TABLES
    );

-- Verify
    COLUMN common_lookup_context  FORMAT A14  HEADING "Common|Lookup Context"
    COLUMN common_lookup_table    FORMAT A12  HEADING "Common|Lookup Table"
    COLUMN common_lookup_column   FORMAT A18  HEADING "Common|Lookup Column"
    COLUMN common_lookup_type     FORMAT A18  HEADING "Common|Lookup Type"
    SELECT   common_lookup_context
    ,        common_lookup_table
    ,        common_lookup_column
    ,        common_lookup_type
    FROM     common_lookup
    WHERE    common_lookup_table IN
            (SELECT table_name
            FROM   user_tables)
    ORDER BY 1, 2, 3;

-- Create COMMON_LOOKUP_COLUMN column values 
UPDATE COMMON_LOOKUP
    SET    COMMON_LOOKUP_COLUMN = 'ADDRESS_TYPE'
    WHERE  COMMON_LOOKUP_CONTEXT = 'MULTIPLE';

-- Verify
    COLUMN common_lookup_context  FORMAT A14  HEADING "Common|Lookup Context"
    COLUMN common_lookup_table    FORMAT A12  HEADING "Common|Lookup Table"
    COLUMN common_lookup_column   FORMAT A18  HEADING "Common|Lookup Column"
    COLUMN common_lookup_type     FORMAT A18  HEADING "Common|Lookup Type"
    SELECT   common_lookup_context
    ,        common_lookup_table
    ,        common_lookup_column
    ,        common_lookup_type
    FROM     common_lookup
    WHERE    common_lookup_table IN
            (SELECT table_name
            FROM   user_tables)
    ORDER BY 1, 2, 3;

-- --------------------------------------------------------
--  Step #4 - Part #4
--  -------
--  Add two new rows to the COMMON_LOOKUP table to support the 'HOME' and 'WORK'
-- --------------------------------------------------------
INSERT INTO COMMON_LOOKUP
    VALUES (
        common_lookup_s1.nextval,
        ' ',
        'HOME',
        'Home Telephone',
        1,
        SYSDATE,
        1,
        SYSDATE,
        'TELEPHONE',
        'TELEPHONE_TYPE',
        NULL
    );

INSERT INTO COMMON_LOOKUP
    VALUES (
        common_lookup_s1.nextval,
        ' ',
        'WORK',
        'Work Telephone',
        1,
        SYSDATE,
        1,
        SYSDATE,
        'TELEPHONE',
        'TELEPHONE_TYPE',
        NULL
    );

-- Verify
COLUMN common_lookup_context  FORMAT A14  HEADING "Common|Lookup Context"
COLUMN common_lookup_table    FORMAT A12  HEADING "Common|Lookup Table"
COLUMN common_lookup_column   FORMAT A18  HEADING "Common|Lookup Column"
COLUMN common_lookup_type     FORMAT A18  HEADING "Common|Lookup Type"
SELECT   common_lookup_context
,        common_lookup_table
,        common_lookup_column
,        common_lookup_type
FROM     common_lookup
WHERE    common_lookup_table IN
          (SELECT table_name
           FROM   user_tables)
ORDER BY 1, 2, 3;

-- --------------------------------------------------------
--  Step #4 - Part #5
--  -------
--  Fix the COMMON_LOOKUP table’s structure 
-- --------------------------------------------------------
ALTER TABLE COMMON_LOOKUP
    DROP COLUMN COMMON_LOOKUP_CONTEXT;

ALTER TABLE COMMON_LOOKUP
    MODIFY (COMMON_LOOKUP_TABLE VARCHAR2(30) CONSTRAINT NN_COMMON_LOOKUP_TABLE NOT NULL);

ALTER TABLE COMMON_LOOKUP
    MODIFY (COMMON_LOOKUP_COLUMN VARCHAR2(30) CONSTRAINT NN_COMMON_LOOKUP_COLUMN NOT NULL);

-- Create new Indexes
CREATE INDEX CLOOKUP_U1
    ON COMMON_LOOKUP(COMMON_LOOKUP_TABLE, COMMON_LOOKUP_COLUMN, COMMON_LOOKUP_TYPE);

-- Verify
SET NULL ''
COLUMN table_name   FORMAT A14
COLUMN column_id    FORMAT 9999
COLUMN column_name  FORMAT A22
COLUMN data_type    FORMAT A12
SELECT   table_name
,        column_id
,        column_name
,        CASE
           WHEN nullable = 'N' THEN 'NOT NULL'
           ELSE ''
         END AS nullable
,        CASE
           WHEN data_type IN ('CHAR','VARCHAR2','NUMBER') THEN
             data_type||'('||data_length||')'
           ELSE
             data_type
         END AS data_type
FROM     user_tab_columns
WHERE    table_name = 'COMMON_LOOKUP'
ORDER BY 2;

-- Verify NN Constraints
COLUMN constraint_name   FORMAT A22  HEADING "Constraint Name"
COLUMN search_condition  FORMAT A36  HEADING "Search Condition" 
COLUMN constraint_type   FORMAT A10  HEADING "Constraint|Type"
SELECT   uc.constraint_name
,        uc.search_condition
,        uc.constraint_type
FROM     user_constraints uc INNER JOIN user_cons_columns ucc
ON       uc.table_name = ucc.table_name
AND      uc.constraint_name = ucc.constraint_name
WHERE    uc.table_name = UPPER('common_lookup')
AND      uc.constraint_type IN (UPPER('c'),UPPER('p'))
ORDER BY uc.constraint_type DESC
,        uc.constraint_name;

-- Verify Natural Key Index
COLUMN sequence_name   FORMAT A22 HEADING "Sequence Name"
COLUMN column_position FORMAT 999 HEADING "Column|Position"
COLUMN column_name     FORMAT A22 HEADING "Column|Name"
SELECT   ui.index_name
,        uic.column_position
,        uic.column_name
FROM     user_indexes ui INNER JOIN user_ind_columns uic
ON       ui.index_name = uic.index_name
AND      ui.table_name = uic.table_name
WHERE    ui.table_name = UPPER('common_lookup')
ORDER BY ui.index_name
,        uic.column_position;

-- --------------------------------------------------------
--  Step #4 - Part #6
--  -------
--  Update incorrect foreign key values in the telephone table.
-- --------------------------------------------------------

-- Find obsolete values
SELECT COMMON_LOOKUP_ID
FROM   COMMON_LOOKUP
WHERE  COMMON_LOOKUP_TABLE = 'ADDRESS'
AND    COMMON_LOOKUP_TYPE = 'HOME';

-- Correct values
SELECT COMMON_LOOKUP_ID
FROM   COMMON_LOOKUP
WHERE  COMMON_LOOKUP_TABLE = 'TELEPHONE'
AND    COMMON_LOOKUP_TYPE = 'HOME';

-- Update
UPDATE TELEPHONE
    SET TELEPHONE_TYPE = (
        SELECT COMMON_LOOKUP_ID
        FROM   COMMON_LOOKUP
        WHERE  COMMON_LOOKUP_TABLE = 'TELEPHONE'
        AND    COMMON_LOOKUP_TYPE = 'HOME'
    )
    WHERE TELEPHONE_TYPE = (
        SELECT COMMON_LOOKUP_ID
        FROM   COMMON_LOOKUP
        WHERE  COMMON_LOOKUP_TABLE = 'ADDRESS'
        AND    COMMON_LOOKUP_TYPE = 'HOME'
    );

UPDATE TELEPHONE
    SET TELEPHONE_TYPE = (
        SELECT COMMON_LOOKUP_ID
        FROM   COMMON_LOOKUP
        WHERE  COMMON_LOOKUP_TABLE = 'TELEPHONE'
        AND    COMMON_LOOKUP_TYPE = 'WORK'
    )
    WHERE TELEPHONE_TYPE = (
        SELECT COMMON_LOOKUP_ID
        FROM   COMMON_LOOKUP
        WHERE  COMMON_LOOKUP_TABLE = 'ADDRESS'
        AND    COMMON_LOOKUP_TYPE = 'WORK'
    );

-- Verify
COLUMN common_lookup_table  FORMAT A14 HEADING "Common|Lookup Table"
COLUMN common_lookup_column FORMAT A14 HEADING "Common|Lookup Column"
COLUMN common_lookup_type   FORMAT A8  HEADING "Common|Lookup|Type"
COLUMN count_dependent      FORMAT 999 HEADING "Count of|Foreign|Keys"
COLUMN count_lookup         FORMAT 999 HEADING "Count of|Primary|Keys"
SELECT   cl.common_lookup_table
,        cl.common_lookup_column
,        cl.common_lookup_type
,        COUNT(a.address_id) AS count_dependent
,        COUNT(DISTINCT cl.common_lookup_table) AS count_lookup
FROM     address a RIGHT JOIN common_lookup cl
ON       a.address_type = cl.common_lookup_id
WHERE    cl.common_lookup_table = 'ADDRESS'
AND      cl.common_lookup_column = 'ADDRESS_TYPE'
AND      cl.common_lookup_type IN ('HOME','WORK')
GROUP BY cl.common_lookup_table
,        cl.common_lookup_column
,        cl.common_lookup_type
UNION
SELECT   cl.common_lookup_table
,        cl.common_lookup_column
,        cl.common_lookup_type
,        COUNT(t.telephone_id) AS count_dependent
,        COUNT(DISTINCT cl.common_lookup_table) AS count_lookup
FROM     telephone t RIGHT JOIN common_lookup cl
ON       t.telephone_type = cl.common_lookup_id
WHERE    cl.common_lookup_table = 'TELEPHONE'
AND      cl.common_lookup_column = 'TELEPHONE_TYPE'
AND      cl.common_lookup_type IN ('HOME','WORK')
GROUP BY cl.common_lookup_table
,        cl.common_lookup_column
,        cl.common_lookup_type;

-- Commit changes to DB.
COMMIT;

SPOOL OFF
